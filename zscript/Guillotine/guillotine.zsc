const HDLD_GUIL="lpg";

const HDLD_GMAG="n15";
// ------------------------------------------------------------
// "La Petite Guillotine" 
//A unique piece, designed by the talented Belgian gunsmith Emeline Leone. Built off the Mouletta 99FX platform it chambers the powerful 9mm Nail Driver Maximum. 
//Originally created for competition shooting, the 9mm NDM cartridge was quickly adapted for use as a self defense cartridge against violent gangs of armored Mimes with the addition of a hardened tungsten penetrator. 
//Before long it found itself being used in the battle against the tyrant's forces with great effect. 
//It is unknown where this pistol came from, or where it's creator is though upon closer inspection you see it is worn, with dried blood and scratch marks on the butt of the pistol. Though some rumors claim she has retired to the Belgian countryside.
//The 9mm NDM traces it's roots back to the .355 Hardball cartridge, the case been cut down from 33mm to 25mm and necked down to accomodate a 7.62mm Projectile instead of the usual 9mm. 
//The projectile features a hardened tungsten penetrator tip sharply pointed to aid in penetration, allowing it to pierce through up to level IIIA armor with ease. 
//The projectile body is made of a dense copper alloy, designed to fragment creating grevious wounds on unarmored targets. Propelling a lightweight 92 grain (6 grams) projectile at a brisk 1850 feet per second (564 m/s). 
//Overall, the cartridge has more power and energy compared to 9mm Parumpudicum, giving it a greater recoil impulse than the standard cartridge. 
//The pistol itself, being based on the Mouletta 99FX uses the same magazines as the original pistol with the same capacity. Just like the stock pistol it is a DA/SA pistol, with the addition of a decocker for carry purposes. 
//The Guillotine features a durable custom frame made of a lightweight Meteora alloy which reduces weight. Though this is offset by the heavier Brigadier style slide and addition of a compensator. 
//The hammer has been skeletonized and all the internals have been replaced with competition grade parts, most notably the trigger which is far lighter and smoother than it's stock counterpart. The magazine release has been made ambidextrous and is extended, the magazine well itself has been beveled to assist in reloading.
//The grips have been personalized for someone with much smaller hands than normal, which makes it rather uncomfortable to hold. Engraved on the left hand of the slide are the words "La Petite Guillotine".
// ------------------------------------------------------------
class HDGuillotine:HDHandgun{
	default{
		+hdweapon.fitsinbackpack
		+hdweapon.reverseguninertia
		scale 0.63;
		weapon.selectionorder 50;
		weapon.slotnumber 2;
		weapon.slotpriority 2.1;
		weapon.kickback 30;
		weapon.bobrangex 0.1;
		weapon.bobrangey 0.6;
		weapon.bobspeed 2.5;
		weapon.bobstyle "normal";
		obituary "%o got capped by %k's pea shooter.";
		inventory.pickupmessage "You picked up La Petite Guillotine!";
		tag "Guillotine";
		hdweapon.refid HDLD_GUIL;
		hdweapon.barrelsize 20,0.3,0.5;
		inventory.maxamount 3;
	}
	override double weaponbulk(){
		int mgg=weaponstatus[GILS_MAG];
		return 30+(mgg<0?0:(ENC_9MAG_LOADED+mgg*ENC_9_LOADED));
	}
	override double gunmass(){
        int mgg=weaponstatus[GILS_MAG];
		return 3.5+(mgg<0?0:0.08*(mgg+1));
	}
	override void failedpickupunload(){
		failedpickupunloadmag(GILS_MAG,"HDNDMMag");
	}
	override string,double getpickupsprite(){
		string spr;
		if(weaponstatus[GILS_CHAMBER]<2){
			 spr="B";
		}else{
				spr="A";
		}
		return "EMLP"..spr.."0",1.;
	}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			int nextmagloaded=sb.GetNextLoadMag(hdmagammo(hpl.findinventory("HDNDMMag")));
			if(nextmagloaded>=15){
				sb.drawimage("GMAGNORM",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM,scale:(1,1));
			}else if(nextmagloaded<1){
				sb.drawimage("GMAGEMPTY",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM,alpha:nextmagloaded?0.6:1.,scale:(1,1));
			}else sb.drawbar(
				"GMAGNORM","GMAGGREY",
				nextmagloaded,15,
				(-46,-3),-1,
				sb.SHADER_VERT,sb.DI_SCREEN_CENTER_BOTTOM
			);
			sb.drawnum(hpl.countinv("HDNDMMag"),-43,-8,sb.DI_SCREEN_CENTER_BOTTOM);
		}
		sb.drawwepnum(hdw.weaponstatus[GILS_MAG],15);
		if(hdw.weaponstatus[GILS_CHAMBER]==2)sb.drawrect(-19,-11,3,1);
	}
	override string gethelptext(){
		return
		WEPHELP_FIRESHOOT
		..WEPHELP_ALTRELOAD.."  Quick-Swap (if available)\n"
		..WEPHELP_RELOAD.."  Reload mag\n"
		..WEPHELP_USE.."+"..WEPHELP_RELOAD.."  Reload chamber\n"
		..WEPHELP_MAGMANAGER
		..WEPHELP_UNLOADUNLOAD
		;
	}
	
		override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc,string whichdot
	){
			double dotoff=max(abs(bob.x),abs(bob.y));
			if(dotoff<30){
				sb.drawimage(
					whichdot,(0,0)+bob*1.18,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
					alpha:0.8-dotoff*0.01,scale:(1.0,1.0)
				);
			}
			sb.drawimage(
				"xh25",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
				scale:(1.5,1.5)
			);
		}
	override void DropOneAmmo(int amt){
		if(owner){
			amt=clamp(amt,1,10);
			if(owner.countinv("HDNDMLoose"))owner.A_DropInventory("HDNDMLoose",amt*15);
			else owner.A_DropInventory("HDNDMMag",amt);
		}
	}
	override void ForceBasicAmmo(){
		owner.A_TakeInventory("HDNDMLoose");
		ForceOneBasicAmmo("HDNDMMag");
	}
	action void A_CheckPistolHand(){
		if(invoker.wronghand)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("EM2GA0");
	}
	states{
	select0:
		EMLG A 0{
			if(!countinv("NulledWeapon"))invoker.wronghand=false;
			A_TakeInventory("NulledWeapon");
			A_CheckPistolHand();
		}
		#### A 0 A_JumpIf(invoker.weaponstatus[GILS_CHAMBER]>0,2);
		#### C 0;
		---- A 1 A_Raise();
		---- A 1 A_Raise(30);
		---- A 1 A_Raise(30);
		---- A 1 A_Raise(24);
		---- A 1 A_Raise(18);
		wait;
	deselect0:
		EMLG A 0 A_CheckPistolHand();
		#### A 0 A_JumpIf(invoker.weaponstatus[GILS_CHAMBER]>0,2);
		#### C 0;
		---- AAA 1 A_Lower();
		---- A 1 A_Lower(18);
		---- A 1 A_Lower(24);
		---- A 1 A_Lower(30);
		wait;

	ready:
		EMLG A 0 A_CheckPistolHand();
		#### A 0 A_JumpIf(invoker.weaponstatus[GILS_CHAMBER]>0,2);
		#### C 0;
		#### # 0 A_SetCrosshair(21);
		#### # 1 A_WeaponReady(WRF_ALL);
		goto readyend;
	user3:
		---- A 0 A_MagManager("HDNDMMag");
		goto ready;
	user2:
	firemode:
		---- A 0;
		goto nope;
	altfire:
		---- A 0{
			invoker.weaponstatus[0]&=~GILF_JUSTUNLOAD;
			if(
				invoker.weaponstatus[GILS_CHAMBER]!=2
				&&invoker.weaponstatus[GILS_MAG]>0
			)setweaponstate("chamber_manual");
		}goto nope;
	chamber_manual:
		---- A 0 A_JumpIf(
			!(invoker.weaponstatus[0]&GILF_JUSTUNLOAD)
			&&(
				invoker.weaponstatus[GILS_CHAMBER]==2
				||invoker.weaponstatus[GILS_MAG]<1
			)
			,"nope"
		);
		#### B 3 offset(0,34);
		#### C 4 offset(0,37){
			A_MuzzleClimb(frandom(0.4,0.5),-frandom(0.6,0.8));
			A_StartSound("weapons/guilchamber2",8);
			int psch=invoker.weaponstatus[GILS_CHAMBER];
			invoker.weaponstatus[GILS_CHAMBER]=0;
			if(psch==2){
				A_EjectCasing("HDNDMLoose",
					frandom(-1,2),
					(frandom(0,0.2),-frandom(2,3),frandom(0.4,0.5)),
					(-2,0,-1)
				);
			}else if(psch==1){
				A_EjectCasing("HDSpentNDM",
                    -frandom(-1,2),
                    (frandom(0.4,0.7),-frandom(6,7),frandom(0.8,1)),
                    (-2,0,-1)
                );
			}
			if(invoker.weaponstatus[GILS_MAG]>0){
				invoker.weaponstatus[GILS_CHAMBER]=2;
				invoker.weaponstatus[GILS_MAG]--;
			}
		}
		#### B 3 offset(0,35);
		goto nope;
	althold:
	hold:
		goto nope;
	fire:
		---- A 0{
			invoker.weaponstatus[0]&=~GILF_JUSTUNLOAD;
			if(invoker.weaponstatus[GILS_CHAMBER]==2)setweaponstate("shoot");
			else if(invoker.weaponstatus[GILS_MAG]>0)setweaponstate("chamber_manual");
		}goto nope;
	shoot:
		#### B 1{
			if(invoker.weaponstatus[GILS_CHAMBER]==2)A_GunFlash();
		}
		#### C 1{
			if(hdplayerpawn(self)){
				hdplayerpawn(self).gunbraced=false;
				A_GiveInventory("IsMoving",1);
			}
			A_MuzzleClimb(
				-frandom(0.8,1.),-frandom(1.2,1.6),
				frandom(0.4,0.5),frandom(0.6,0.8)
			);
		}
		#### C 0{
			A_EjectCasing("HDSpentNDM",
                frandom(-1,2),
                (frandom(0.4,0.7),-frandom(6,7),frandom(0.8,1))
                );
			invoker.weaponstatus[GILS_CHAMBER]=0;
			if(invoker.weaponstatus[GILS_MAG]<1){
				A_StartSound("weapons/guildry",8,CHANF_OVERLAP,0.9);
				setweaponstate("nope");
			}
		}
		#### B 1{
			A_WeaponReady(WRF_NOFIRE);
			invoker.weaponstatus[GILS_CHAMBER]=2;
			invoker.weaponstatus[GILS_MAG]--;
			A_Refire();
		}goto ready;
	flash:
		EM2F A 0 A_JumpIf(invoker.wronghand,2);
		EMLF A 0;
		---- A 1 bright{
			HDFlashAlpha(64);
			A_Light1();
			let bbb=HDBulletActor.FireBullet(self,"HDB_NDM",spread:2.,speedfactor:frandom(0.97,1.03));
			if(
				frandom(0,ceilingz-floorz)<bbb.speed*0.3
			)A_AlertMonsters(256);

			invoker.weaponstatus[GILS_CHAMBER]=1;
			A_ZoomRecoil(0.985);
			A_MuzzleClimb(-frandom(1.0,1.8),-frandom(1.8,2.2));
		}
		---- A 0 A_StartSound("weapons/guillotine",CHAN_WEAPON);
		---- A 0 A_Light0();
		stop;
	unload:
		---- A 0{
			invoker.weaponstatus[0]|=GILF_JUSTUNLOAD;
			if(invoker.weaponstatus[GILS_MAG]>=0)setweaponstate("unmag");
		}goto chamber_manual;
	loadchamber:
		---- A 0 A_JumpIf(invoker.weaponstatus[GILS_CHAMBER]>0,"nope");
		---- A 1 offset(0,36) A_StartSound("weapons/pocket",9);
		---- A 1 offset(2,40);
		---- A 1 offset(2,50);
		---- A 1 offset(3,60);
		---- A 2 offset(5,90);
		---- A 2 offset(7,80);
		---- A 2 offset(10,90);
		#### C 2 offset(8,96);
		#### C 3 offset(6,88){
			if(countinv("HDNDMLoose")){
				A_TakeInventory("HDNDMLoose",1,TIF_NOTAKEINFINITE);
				invoker.weaponstatus[GILS_CHAMBER]=2;
				A_StartSound("weapons/guilchamber1",8);
			}
		}
		#### B 2 offset(5,76);
		#### B 1 offset(4,64);
		#### B 1 offset(3,56);
		#### B 1 offset(2,48);
		#### B 2 offset(1,38);
		#### B 3 offset(0,34);
		goto readyend;
	reload:
		---- A 0{
			invoker.weaponstatus[0]&=~GILF_JUSTUNLOAD;
			bool nomags=HDMagAmmo.NothingLoaded(self,"HDNDMMag");
			if(invoker.weaponstatus[GILS_MAG]>=15)setweaponstate("nope");
			else if(
				invoker.weaponstatus[GILS_MAG]<1
				&&(
					pressinguse()
					||nomags
				)
			){
				if(
					countinv("HDNDMLoose")
				)setweaponstate("loadchamber");
				else setweaponstate("nope");
			}else if(nomags)setweaponstate("nope");
		}goto unmag;
	unmag:
		---- A 1 offset(0,34) A_SetCrosshair(21);
		---- A 1 offset(1,38);
		---- A 2 offset(2,42);
		---- A 3 offset(3,46) A_StartSound("weapons/guilmagout",8,CHANF_OVERLAP);
		---- A 0{
			int pmg=invoker.weaponstatus[GILS_MAG];
			invoker.weaponstatus[GILS_MAG]=-1;
			if(pmg<0)setweaponstate("magout");
			else if(
				(!PressingUnload()&&!PressingReload())
				||A_JumpIfInventory("HDNDMMag",0,"null")
			){
				HDMagAmmo.SpawnMag(self,"HDNDMMag",pmg);
				setweaponstate("magout");
			}
			else{
				HDMagAmmo.GiveMag(self,"HDNDMMag",pmg);
				A_StartSound("weapons/pocket",9);
				setweaponstate("pocketmag");
			}
		}
	pocketmag:
		---- AAA 5 offset(0,46) A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		goto magout;
	magout:
		---- A 0{
			if(invoker.weaponstatus[0]&GILF_JUSTUNLOAD)setweaponstate("reloadend");
			else setweaponstate("loadmag");
		}

	loadmag:
		---- A 4 offset(0,46) A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		---- A 0 A_StartSound("weapons/pocket",9);
		---- A 5 offset(0,46) A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		---- A 3;
		---- A 0{
			let mmm=hdmagammo(findinventory("HDNDMMag"));
			if(mmm){
				invoker.weaponstatus[GILS_MAG]=mmm.TakeMag(true);
				A_StartSound("weapons/guilmagin",8);
			}
		}
		goto reloadend;

	reloadend:
		---- A 2 offset(3,46);
		---- A 1 offset(2,42);
		---- A 1 offset(2,38);
		---- A 1 offset(1,34);
		---- A 0 A_JumpIf(!(invoker.weaponstatus[0]&GILF_JUSTUNLOAD),"chamber_manual");
		goto nope;

	user1:
	altreload:
	swappistols:
		---- A 0 A_SwapHandguns();
		---- A 0{
			bool id=(Wads.CheckNumForName("id",0)!=-1);
			bool offhand=invoker.wronghand;
			bool lefthanded=(id!=offhand);
			if(lefthanded){
				A_Overlay(1025,"raiseleft");
				A_Overlay(1026,"lowerright");
			}else{
				A_Overlay(1025,"raiseright");
				A_Overlay(1026,"lowerleft");
			}
		}
		TNT1 A 5;
		EMLG A 0 A_CheckPistolHand();
		goto nope;
	lowerleft:
		EMLG A 0 A_JumpIf(Wads.CheckNumForName("id",0)!=-1,2);
		EM2G A 0;
		#### B 1 offset(-6,38);
		#### B 1 offset(-12,48);
		#### B 1 offset(-20,60);
		#### B 1 offset(-34,76);
		#### B 1 offset(-50,86);
		stop;
	lowerright:
		EM2G A 0 A_JumpIf(Wads.CheckNumForName("id",0)!=-1,2);
		EMLG A 0;
		#### B 1 offset(6,38);
		#### B 1 offset(12,48);
		#### B 1 offset(20,60);
		#### B 1 offset(34,76);
		#### B 1 offset(50,86);
		stop;
	raiseleft:
		EMLG A 0 A_JumpIf(Wads.CheckNumForName("id",0)!=-1,2);
		EM2G A 0;
		#### A 1 offset(-50,86);
		#### A 1 offset(-34,76);
		#### A 1 offset(-20,60);
		#### A 1 offset(-12,48);
		#### A 1 offset(-6,38);
		stop;
	raiseright:
		EM2G A 0 A_JumpIf(Wads.CheckNumForName("id",0)!=-1,2);
		EMLG A 0;
		#### A 1 offset(50,86);
		#### A 1 offset(34,76);
		#### A 1 offset(20,60);
		#### A 1 offset(12,48);
		#### A 1 offset(6,38);
		stop;
	whyareyousmiling:
		#### B 1 offset(0,48);
		#### B 1 offset(0,60);
		#### B 1 offset(0,76);
		TNT1 A 7;
		EMLG A 0{
			invoker.wronghand=!invoker.wronghand;
			A_CheckPistolHand();
		}
		#### B 1 offset(0,76);
		#### B 1 offset(0,60);
		#### B 1 offset(0,48);
		goto nope;


	spawn:
		EMLP AB -1 nodelay{
			if(invoker.weaponstatus[GILS_CHAMBER]<2){
			frame=1;
			}else{
			frame=0;
			}
		}stop;
	}
	override void initializewepstats(bool idfa){
		weaponstatus[GILS_MAG]=15;
		weaponstatus[GILS_CHAMBER]=2;
	}
}

class HDNDMMag:HDMagAmmo{
	default{
		//$Category "Ammo/Hideous Destructor/"
		//$Title "Pistol Magazine"
		//$Sprite "GMAGA0"

		hdmagammo.maxperunit 15;
		hdmagammo.roundtype "HDNDMLoose";
		hdmagammo.roundbulk ENC_9_LOADED;
		hdmagammo.magbulk ENC_9MAG_EMPTY;
		tag "9mm NDM magazine";
		inventory.pickupmessage "Picked up a magazine of 9mm NDM.";
		hdpickup.refid HDLD_GMAG;
	}
	override string,string,name,double getmagsprite(int thismagamt){
		string magsprite=(thismagamt>0)?"GMAGNORM":"GMAGEMPTY";
		return magsprite,"NRNDA0","HDNDMLoose",0.6;
	}
	override void GetItemsThatUseThis(){
		itemsthatusethis.push("HDGuillotine");
	}
	states{
	spawn:
		GMAG A -1;
		stop;
	spawnempty:
		GMAG B -1{
			brollsprite=true;brollcenter=true;
			roll=randompick(0,0,0,0,2,2,2,2,1,3)*90;
		}stop;
	}
}

class HDNDMEmptyMag:IdleDummy{
	override void postbeginplay(){
		super.postbeginplay();
		HDMagAmmo.SpawnMag(self,"HDNDMMag",0);
		destroy();
	}
}


class GuillotineSpawn:IdleDummy{
	override void postbeginplay(){
		super.postbeginplay();

		bool _; Actor wep;
		[_, wep] = A_SpawnItemEx("HDGuillotine",flags:SXF_NOCHECKPOSITION);
		HDF.TransferSpecials(self, wep);

		A_SpawnItemEx("HDNDMMag",flags:SXF_NOCHECKPOSITION);
		A_SpawnItemEx("HDNDMMag",-1,-1,flags:SXF_NOCHECKPOSITION);
		destroy();
	}
}

enum pistolstatus{
	GILF_JUSTUNLOAD=1,

	GILS_FLAGS=0,
	GILS_MAG=1,
	GILS_CHAMBER=2, //0 empty, 1 spent, 2 loaded
};
