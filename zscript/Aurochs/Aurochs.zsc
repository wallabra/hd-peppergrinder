// ------------------------------------------------------------
// Single Action Revolver
// ------------------------------------------------------------

/*
const HDLD_420="4fr";
const HDLD_069BORE="69b";
const ENC_420=1;
const ENC_420_LOADED=0.6;
const ENC_069BORE=0.6;
const ENC_069BORE_LOADED=0.4;

normal barrel speedfactor: 1
bluntline speedfactor: 1.09
roach speedfactor: 0.8

*/
const HDLD_AUROCHS="aur";
class HDAurochs:HDHandgun{
	bool cylinderopen; //don't use weaponstatus since it shouldn't be saved anyway
	default{
		+hdweapon.fitsinbackpack
		+hdweapon.reverseguninertia
		scale 0.5;
		weapon.selectionorder 49;
		weapon.slotnumber 2;
		weapon.slotpriority 3;
		weapon.kickback 30;
		weapon.bobrangex 0.1;
		weapon.bobrangey 0.6;
		weapon.bobspeed 2.5;
		weapon.bobstyle "normal";
		obituary "%o got capped by %k's six-pea shooter.";
		inventory.pickupmessage "You got the single action revolver!";
		tag "Aurochs .451 Revolver";
		hdweapon.refid HDLD_AUROCHS;
		hdweapon.barrelsize 22,0.3,0.5;
		hdweapon.loadoutcodes "
			\cushotshells - Start with .066 bore shotshells loaded.
			\cuclockwise - Make revolver rotate clockwise when cocking.
			\cualt - Give revolver alternate color scheme.";
	}

	
	override void loadoutconfigure(string input){
		int shotshells=getloadoutvar(input,"shotshells",1);
		if(shotshells>=0){
			weaponstatus[AURY_CYL1]=AURY_SIXNINEBORE;
			weaponstatus[AURY_CYL2]=AURY_SIXNINEBORE;
			weaponstatus[AURY_CYL3]=AURY_SIXNINEBORE;
			weaponstatus[AURY_CYL4]=AURY_SIXNINEBORE;
			weaponstatus[AURY_CYL5]=AURY_SIXNINEBORE;
			weaponstatus[AURY_CYL6]=AURY_SIXNINEBORE;
		}
		/*int bluntline=getloadoutvar(input,"bluntline",1);
		if(bluntline>=0){
			weaponstatus[AURF_BARREL]=1;
		}
		int roach=getloadoutvar(input,"roach",1);
		if(roach>=0){
			weaponstatus[AURF_BARREL]=2;
		}*/
		
		int clockwise = getloadoutvar(input,"clockwise",1);
		if (clockwise > 0)
		{
			weaponstatus[0] |= AURF_CLOCKWISE;
		}
		int alternate=getloadoutvar(input,"alt",1);
		if(alternate > 0){
			weaponstatus[0]|=AURF_ALTERNATE;
		}
	}
	override double gunmass(){
		double blk=0;
		for(int i=AURY_CYL1;i<=AURY_CYL6;i++){
			int wi=weaponstatus[i];
			if(wi==AURY_FOURTWENTY)blk+=0.12;
			else if(wi==AURY_SIXNINEBORE)blk+=0.1;
		}
		return blk+4;
	}
	override double weaponbulk(){
		double blk=0;
		for(int i=AURY_CYL1;i<=AURY_CYL6;i++){
			int wi=weaponstatus[i];
			if(wi==AURY_FOURTWENTY)blk+=ENC_420_LOADED;
			else if(wi==AURY_SIXNINEBORE)blk+=ENC_069BORE_LOADED;
		}
		/*int barrelweight;
		switch(weaponstatus[AURF_BARREL]){
			case 0: barrelweight = 34; break;
			case 1: barrelweight = 38; break;
			case 2: barrelweight = 28;  break;
		}*/
		
		return blk+34;
	}
	override string,double getpickupsprite(){
		string spr;
		int alt=weaponstatus[0]&AURF_ALTERNATE;
		if (!alt){
			spr="A";
		}else{
			spr="2";
		}
		return spr.."URPA0",1.;
	}

	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			sb.drawimage("420BA0",(-47,-10),sb.DI_SCREEN_CENTER_BOTTOM,scale:(2.1,2.55));
			sb.drawnum(hpl.countinv("HDAurochsAmmo"),-44,-8,sb.DI_SCREEN_CENTER_BOTTOM);
			int ninemil=hpl.countinv("HD069BoreAmmo");
			if(ninemil>0){
				sb.drawimage("42BRA0",(-64,-10),sb.DI_SCREEN_CENTER_BOTTOM,scale:(2.1,2.1));
				sb.drawnum(ninemil,-60,-8,sb.DI_SCREEN_CENTER_BOTTOM);
			}
		}
		int plf=hpl.player.getpsprite(PSP_WEAPON).frame;
		for(int i=AURY_CYL1;i<=AURY_CYL6;i++){
			double drawangle=i*(360./6.)-150;
			vector2 cylpos;
			{
				cylpos=(-25,-20);
			}
			double cdrngl=cos(drawangle);
			double sdrngl=sin(drawangle);
			if(
				!cylinderopen
				&&sb.hud_aspectscale.getbool()
			){
				cdrngl*=1.1;
				sdrngl*=(1./1.1);
			}
			color cylindercolor;
			switch(hdw.weaponstatus[i]){
				case 1: case 2: cylindercolor = color(255,112,112,112); break;
				case 3: case 4: cylindercolor = color(255,19,98,4); break;
				default:cylindercolor = color(200,30,26,24); break;
			}
			vector2 drawpos=cylpos+(cdrngl,sdrngl)*5;
			sb.fill(
				/*hdw.weaponstatus[i]>0?
				color(255,19,98,4)
				:color(200,30,26,24),*/
				cylindercolor,
				drawpos.x,
				drawpos.y,
				3,3,
				sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT
			);
		}
	}
	override string gethelptext(){
		if(cylinderopen)return
		WEPHELP_FIRE.." Close loading gate\n"
		..WEPHELP_ALTFIRE.." Cycle cylinder \(Hold "..WEPHELP_ZOOM.." to reverse\)\n"
		..WEPHELP_UNLOAD.." Hit extractor \(double-tap to dump live rounds\)\n"
		..WEPHELP_RELOAD.." Load round \(Hold "..WEPHELP_FIREMODE.." to force using .069 bore\)\n"
		;
		return
		WEPHELP_FIRESHOOT
		..WEPHELP_ALTFIRE.." Pull back hammer\n"
		..WEPHELP_ALTRELOAD.."/"..WEPHELP_FIREMODE.."  Quick-Swap (if available)\n"
		..WEPHELP_UNLOAD.."/"..WEPHELP_RELOAD.." Open loading gate\n"
		;
	}
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc
	){
		bool alt = weaponstatus[0]&AURF_ALTERNATE;
		if(HDAurochs(hdw).cylinderopen)return;

		int cx,cy,cw,ch;
		[cx,cy,cw,ch]=screen.GetClipRect();
		vector2 scc;
		vector2 bobb=bob*1.3;

		sb.SetClipRect(
			-8+bob.x,-9+bob.y,16,15,
			sb.DI_SCREEN_CENTER
		);
		scc=(0.9,0.9);

		sb.drawimage(
			alt?"2URFRNT":"AURFRNT",(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			alpha:0.9,scale:scc
		);
		sb.SetClipRect(cx,cy,cw,ch);
		sb.drawimage(
			alt?"2URBACK":"AURBACK",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			scale:scc
		);
	}
	override void DropOneAmmo(int amt){
		if(owner){
			amt=clamp(amt,18,60);
			if(owner.countinv("HDAurochsAmmo"))owner.A_DropInventory("HDAurochsAmmo",amt);
			else owner.A_DropInventory("HD069BoreAmmo",amt);
		}
	}
	override void ForceBasicAmmo(){
		owner.A_SetInventory("HDAurochsAmmo",1);
	}
	override void initializewepstats(bool idfa){
		weaponstatus[AURY_CYL1]=AURY_FOURTWENTY;
		weaponstatus[AURY_CYL2]=AURY_FOURTWENTY;
		weaponstatus[AURY_CYL3]=AURY_FOURTWENTY;
		weaponstatus[AURY_CYL4]=AURY_FOURTWENTY;
		weaponstatus[AURY_CYL5]=AURY_FOURTWENTY;
		weaponstatus[AURY_CYL6]=AURY_FOURTWENTY;
	}
	
	
	/*action bool CheckAlternate(){
		bool alternate=invoker.weaponstatus[0]&AURF_ALTERNATE;
		alternate=(alternate)||(!alternate);
		return alternate;
	}*/
	
	action bool HoldingRightHanded(){
		bool righthanded=invoker.wronghand;
		righthanded=
		(
			righthanded
			&&Wads.CheckNumForName("id",0)!=-1
		)||(
			!righthanded
			&&Wads.CheckNumForName("id",0)==-1
		);
		return righthanded;
	}
	
	action void A_CheckAltAurochs(){
		if(invoker.weaponstatus[0]&AURF_ALTERNATE){
			player.getpsprite(PSP_WEAPON).sprite=getspriteindex("2URGA0");
		}else{
			player.getpsprite(PSP_WEAPON).sprite=getspriteindex("AURGA0");
		}
		A_CheckRevolverHand();
	}
	
	action void A_CheckRevolverHand(){
		/*bool righthanded=HoldingRightHanded();
		if(righthanded)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("ARLGA0");
		else player.getpsprite(PSP_WEAPON).sprite=getspriteindex("AURGA0");*/
		if(invoker.wronghand){
			if(invoker.weaponstatus[0]&AURF_ALTERNATE){
				player.getpsprite(PSP_WEAPON).sprite=getspriteindex("2RLGA0");
			}else{
				player.getpsprite(PSP_WEAPON).sprite=getspriteindex("ARLGA0");
			}
		}
	}
	action void A_RotateCylinder(bool clockwise=true){
		invoker.RotateCylinder(clockwise);
		A_StartSound("weapons/auryrotate",8,CHANF_OVERLAP);
		switch(invoker.weaponstatus[AURY_CYL2]){
			default: A_ClearOverlays(); break;
			case 1: A_Overlay(100,"round2spent"); break;
			case 2: A_Overlay(100,"round2"); break;
			case 3: A_Overlay(100,"round1spent"); break;
			case 4: A_Overlay(100,"round1"); break;
		}
	}
	action void A_RotateCylinderCock(bool clockwise=false){
		invoker.RotateCylinder(clockwise);
		A_StartSound("weapons/aurypull",8);
	}
	void RotateCylinder(bool clockwise=true){
		if(clockwise){
			int cylbak=weaponstatus[AURY_CYL1];
			weaponstatus[AURY_CYL1]=weaponstatus[AURY_CYL6];
			weaponstatus[AURY_CYL6]=weaponstatus[AURY_CYL5];
			weaponstatus[AURY_CYL5]=weaponstatus[AURY_CYL4];
			weaponstatus[AURY_CYL4]=weaponstatus[AURY_CYL3];
			weaponstatus[AURY_CYL3]=weaponstatus[AURY_CYL2];
			weaponstatus[AURY_CYL2]=cylbak;
		}else{
			int cylbak=weaponstatus[AURY_CYL1];
			weaponstatus[AURY_CYL1]=weaponstatus[AURY_CYL2];
			weaponstatus[AURY_CYL2]=weaponstatus[AURY_CYL3];
			weaponstatus[AURY_CYL3]=weaponstatus[AURY_CYL4];
			weaponstatus[AURY_CYL4]=weaponstatus[AURY_CYL5];
			weaponstatus[AURY_CYL5]=weaponstatus[AURY_CYL6];
			weaponstatus[AURY_CYL6]=cylbak;
		}
	}
	action void A_LoadRound(){
		if(invoker.weaponstatus[AURY_CYL2]>0)return;
		bool usesixnine=(
			player.cmd.buttons&BT_FIREMODE
			||!countinv("HDAurochsAmmo")
		);
		if(usesixnine&&!countinv("HD069BoreAmmo"))return;
		class<inventory>ammotype=usesixnine?"HD069BoreAmmo":"HDAurochsAmmo";
		A_TakeInventory(ammotype,1,TIF_NOTAKEINFINITE);
		invoker.weaponstatus[AURY_CYL2]=usesixnine?AURY_SIXNINEBORE:AURY_FOURTWENTY;
		A_StartSound(usesixnine?"weapons/auryshellinsert":"weapons/auryinsert",8,CHANF_OVERLAP);
		switch(usesixnine){
			case 0:
			A_Overlay(100, "round1");
			break;
			case 1:
			A_Overlay(100, "round2");
			break;
		}
	}
	action void A_OpenCylinder(){
		A_StartSound("weapons/auryopen",8);
		invoker.weaponstatus[0]&=~AURF_COCKED;
		invoker.cylinderopen=true;
		A_SetHelpText();
		switch(invoker.weaponstatus[AURY_CYL2]){
			case 0:
				//A_Log("Squart");
				A_ClearOverlays();
				break;
			case 1:
				//A_Log("Penis");
				A_Overlay(100,"round2spent");
				break;
			case 2:
				//A_Log("Fucker");
				A_Overlay(100,"round2");
				break;
			case 3:
				//A_Log("Anal");
				A_Overlay(100,"round1spent");
				break;
			case 4:
				//A_Log("Shit");
				A_Overlay(100,"round1");
				break;
		}
	}
	action void A_CloseCylinder(){
		A_StartSound("weapons/auryclose",8);
		invoker.cylinderopen=false;
		A_SetHelpText();
		A_ClearOverlays(); 
	}
	action void A_HitExtractor(){
		double cosp=cos(pitch);
		for(int i=AURY_CYL2;i<=AURY_CYL2;i++){
			int thischamber=invoker.weaponstatus[i];
			if(thischamber<1)continue;
			if(
				thischamber==AURY_SIXNINEBORESPENT
				||thischamber==AURY_FOURTWENTYSPENT
			){
				actor aaa=spawn(
					thischamber==AURY_FOURTWENTYSPENT?"HDSpent420"
						:"HDSpent069Bore",
					(pos.xy,pos.z+height-10)
					+(cosp*cos(angle),cosp*sin(angle),sin(pitch))*7,
					ALLOW_REPLACE
				);
				aaa.vel=vel+(frandom(-1,1),frandom(-1,1),-1);
				aaa.angle=angle;
				invoker.weaponstatus[i]=0;
				A_ClearOverlays();
			}
			
		}
		A_StartSound("weapons/auryeject",8,CHANF_OVERLAP);
	}
	action void A_ExtractAll(){
		double cosp=cos(pitch);
		bool gotany=false;
		for(int i=AURY_CYL2;i<=AURY_CYL2;i++){
			int thischamber=invoker.weaponstatus[i];
			if(thischamber<1)continue;
			if(
				thischamber==AURY_FOURTWENTY||thischamber==AURY_SIXNINEBORE
			){
				//give or spawn either 9mm or 355
				class<inventory>ammotype=
					thischamber==AURY_FOURTWENTY?
					"HDAurochsAmmo":"HD069BoreAmmo";
				if(A_JumpIfInventory(ammotype,0,"null")){
					actor aaa=spawn(ammotype,
						(pos.xy,pos.z+height-14)
						+(cosp*cos(angle),cosp*sin(angle),sin(pitch)-2)*3,
						ALLOW_REPLACE
					);
					aaa.vel=vel+(frandom(-1,1),frandom(-1,1),-1);
				}else{
					A_GiveInventory(ammotype,1);
					gotany=true;
				}
				invoker.weaponstatus[i]=0;
				A_ClearOverlays();
			}
		}
		if(gotany)A_StartSound("weapons/pocket",9);
	}
	
	action void A_FireRevolver(){
		invoker.weaponstatus[0]&=~AURF_COCKED;
		int cyl=invoker.weaponstatus[AURY_CYL1];
		if(
			cyl!=AURY_FOURTWENTY
			&&cyl!=AURY_SIXNINEBORE
		){
			A_StartSound("weapons/auryempty",8,CHANF_OVERLAP);
			return;
		}
		invoker.weaponstatus[AURY_CYL1]--;
		bool fourtwenty=cyl==AURY_FOURTWENTY;
		
		int barrelspeed;
		
		/*switch(invoker.weaponstatus[AURF_BARREL]){
			default: barrelspeed = 1; break;
			case 0: barrelspeed = 1; break;
			case 1: barrelspeed = 1.1; break;
			case 2: barrelspeed = 0.8;  break;
		}*/
		
		
		let bbb=HDBulletActor.FireBullet(self,fourtwenty?"HDB_420":"HDB_000",spread:fourtwenty?1.:2.,speedfactor:fourtwenty?1:1.0+0.02857*1,amount:fourtwenty?1:5);
		if(
			frandom(0,ceilingz-floorz)<bbb.speed*(fourtwenty?0.4:0.3)
		)A_AlertMonsters(fourtwenty?512:256);

		A_GunFlash();
		A_Light1();
		A_ZoomRecoil(0.995);
		HDFlashAlpha(fourtwenty?72:64);
		if(hdplayerpawn(self)){
			hdplayerpawn(self).gunbraced=false;
		}
		if(fourtwenty){
			A_ZoomRecoil(0.944);
			A_MuzzleClimb(-frandom(1.5,2.2),-frandom(2.4,2.8));
			A_StartSound("weapons/auryshoot",CHAN_WEAPON,CHANF_OVERLAP,0.5);
			A_StartSound("weapons/auryshoot",CHAN_WEAPON,CHANF_OVERLAP);
			A_StartSound("weapons/deinoblast2",CHAN_WEAPON,CHANF_OVERLAP,0.4);
		}else{
			A_MuzzleClimb(-frandom(0.8,1.2),-frandom(0.8,1.8));
			A_ZoomRecoil(0.99);
			A_StartSound("weapons/auryshotgun",CHAN_WEAPON,CHANF_OVERLAP,0.5);
			A_StartSound("weapons/auryshotgun",CHAN_WEAPON,CHANF_OVERLAP);
		}
	}
	int cooldown;
	action void A_ReadyOpen(){
		//i could move this to an action function but i am coding this at 2 in the morning so i won't, not yet at least
		bool alternate=invoker.weaponstatus[0]&AURF_ALTERNATE;
		if(!alternate)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("AURRC0");
		else player.getpsprite(PSP_WEAPON).sprite=getspriteindex("2URRC0");
		A_WeaponReady(WRF_NOFIRE|WRF_ALLOWUSER3);
		if(justpressed(BT_ALTATTACK))setweaponstate("open_rotatecylinder");
		else if(justpressed(BT_RELOAD)){
			if(
				(
					invoker.weaponstatus[AURY_CYL1]>0
					&&invoker.weaponstatus[AURY_CYL2]>0
					&&invoker.weaponstatus[AURY_CYL3]>0
					&&invoker.weaponstatus[AURY_CYL4]>0
					&&invoker.weaponstatus[AURY_CYL5]>0
					&&invoker.weaponstatus[AURY_CYL6]>0
				)||(
					!countinv("HD069BoreAmmo")
					&&!countinv("HDAurochsAmmo")
				)
			)setweaponstate("open_closecylinder");
			else setweaponstate("open_loadround");
		}else if(justpressed(BT_ATTACK))setweaponstate("open_closecylinder");
		else if(justpressed(BT_UNLOAD)){
			if(!invoker.cooldown){
				setweaponstate("open_dumpcylinder");
				invoker.cooldown=3;
			}else{
				setweaponstate("open_dumpcylinder_all");
			}
		}
		if(invoker.cooldown>0)invoker.cooldown--;
	}
	action void A_RoundReady(int rndnm){
		int gunframe=-1;
		if(invoker.weaponstatus[rndnm]>0)gunframe=player.getpsprite(PSP_WEAPON).frame;
		let thissprite=player.getpsprite(AURY_OVRCYL+rndnm);
		/*switch(gunframe){
		case 4: //E
			thissprite.frame=0;
			break;
		case 5: //F
			thissprite.frame=1;
			break;
		case 6: //G
			thissprite.frame=pressingzoom()?4:2;
			break;
		default:
			thissprite.sprite=getspriteindex("TNT1A0");
			thissprite.frame=0;
			return;break;
		}*/
	}
	action void A_CockHammer(bool yes=true){
		if(yes)invoker.weaponstatus[0]|=AURF_COCKED;
		else invoker.weaponstatus[0]&=~AURF_COCKED;
	}


/*
	A normal ready
	B ready cylinder midframe
	C hammer fully cocked (maybe renumber these lol)
	D recoil frame
	E cylinder swinging out - left hand passing to right
	F cylinder swung out - held in right hand, working chamber in middle
	G cylinder swung out midframe
*/
	states{
	select0:
		AURG A 0;
		#### A 0{
			if(!countinv("NulledWeapon"))invoker.wronghand=false;
			A_TakeInventory("NulledWeapon");
			A_CheckAltAurochs();
			invoker.cylinderopen=false;
			invoker.weaponstatus[0]&=~AURF_COCKED;
			//uncock all spare revolvers
			if(findinventory("SpareWeapons")){
				let spw=SpareWeapons(findinventory("SpareWeapons"));
				for(int i=0;i<spw.weapontype.size();i++){
					if(spw.weapontype[i]==invoker.getclassname()){
						string spw2=spw.weaponstatus[i];
						string spw1=spw2.left(spw2.indexof(","));
						spw2=spw2.mid(spw2.indexof(","));
						int stat0=spw1.toint();
						stat0&=~AURF_COCKED;
						spw.weaponstatus[i]=stat0..spw2;
					}
				}
			}
		}
		---- A 1 A_Raise();
		---- A 1 A_Raise(40);
		---- A 1 A_Raise(40);
		---- A 1 A_Raise(25);
		---- A 1 A_Raise(20);
		wait;
	deselect0:
		AURG A 0 A_CheckAltAurochs();
		#### D 0 A_JumpIf(!invoker.cylinderopen,"deselect0a");
		AURG E 1 A_CloseCylinder();
		AURG E 1;
		AURG A 0 A_CheckAltAurochs();
		goto deselect0a;
	deselect0a:
		#### AD 1 A_Lower();
		---- A 1 A_Lower(20);
		---- A 1 A_Lower(34);
		---- A 1 A_Lower(50);
		wait;
	ready:
		AURG A 0 A_CheckAltAurochs();
		---- A 0 A_JumpIf(invoker.cylinderopen,"readyopen");
		#### F 0 A_JumpIf(invoker.weaponstatus[0]&AURF_COCKED,2);
		#### A 0;
		---- A 1 A_WeaponReady(WRF_ALLOWRELOAD|WRF_ALLOWUSER1|WRF_ALLOWUSER2|WRF_ALLOWUSER3|WRF_ALLOWUSER4);
		goto readyend;
	fire:
		#### A 0 {
			if(invoker.weaponstatus[0]&AURF_COCKED){
				setweaponstate("hammertime");
			}else{
				setweaponstate("nope");
			}
		}
		#### G 1 offset(0,34);
		#### G 2 offset(0,36) A_RotateCylinder(invoker.weaponstatus[0]&AURF_CLOCKWISE ? true : false);
		#### A 0 offset(0,32);
	hammertime:
		#### A 0 A_ClearRefire();
		#### A 1 A_FireRevolver();
		goto nope;
	firerecoil:
		#### g 2;
		#### A 0;
		goto nope;
	flash:
		AURF A 1 bright;
		---- A 0 A_Light0();
		---- A 0 setweaponstate("firerecoil");
		stop;
		ARLG ABCD 0;
		stop;
	altfire:
		---- A 0 A_JumpIf(invoker.weaponstatus[0]&AURF_COCKED,"uncock");
		#### BC 2 offset(0,34) A_ClearRefire();
		#### D 3 offset(0,36) A_RotateCylinderCock(invoker.weaponstatus[0]&AURF_CLOCKWISE ? true : false);
	cockedback:
		#### D 1 A_WeaponReady(WRF_NOFIRE);
		#### D 0 A_JumpIf(!pressingaltfire(),"cocked");
		#### D 0 A_CockHammer();
		#### D 0 A_JumpIf(pressingfire(),"fire");
		loop;
	cocked:
		#### E 3;
		#### F 0 A_CockHammer();
		---- F 0 A_JumpIf(pressingaltfire(),"nope");
		goto readyend;
	uncock:
		#### E 3 offset(0,38);
		#### D 3 offset(0,34);
		#### CB 2 offset(0,34);
		#### A 2 offset(0,36) A_StartSound("weapons/deinocyl",8,CHANF_OVERLAP);
		#### A 0 A_CockHammer(false);
		goto nope;
	reload:
	unload:
		#### C 0 A_JumpIf(!(invoker.weaponstatus[0]&AURF_COCKED),3);
		#### G 2 offset(0,35)A_CockHammer(false);
		#### A 2 offset(0,33);
		#### A 1 {
			bool alternate=invoker.weaponstatus[0]&AURF_ALTERNATE;
			if(player.getpsprite(PSP_WEAPON).sprite!=getspriteindex(alternate?"2URGA0":"AURGA0")){
				setweaponstate("openslow");
			}
		}
		AURR B 0{
			bool alternate=invoker.weaponstatus[0]&AURF_ALTERNATE;
			if(!alternate)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("AURRC0");
			else player.getpsprite(PSP_WEAPON).sprite=getspriteindex("2URRC0");
		}
		#### B 1;
		#### C 1 A_OpenCylinder();
		goto readyopen;
	openslow:
		AURR B 0{
			bool alternate=invoker.weaponstatus[0]&AURF_ALTERNATE;
			if(!alternate)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("AURRC0");
			else player.getpsprite(PSP_WEAPON).sprite=getspriteindex("2URRC0");
		}
		#### B 1 offset(2,39);
		#### B 1 offset(4,50);
		#### B 1 offset(8,64);
		#### B 1 offset(10,86);
		#### B 1 offset(12,96);
		#### C 1 offset(-7,66);
		#### C 1 offset(-6,56);
		#### C 1 offset(-2,40);
		#### C 1 offset(0,32);
		#### C 1 A_OpenCylinder();
		goto readyopen;
	readyopen:
		#### C 1 A_ReadyOpen();
		goto readyend;
	open_rotatecylinder:
		#### D 1 A_RotateCylinder(invoker.weaponstatus[0]&AURF_CLOCKWISE ? !pressingzoom() : pressingzoom());
		#### C 2 A_JumpIf(!pressingaltfire(),"readyopen");
		goto readyopen;
	open_loadround:
		#### C 2 offset(0,32);
		#### E 1 A_LoadRound();
		goto readyopen;
	open_closecylinder:
		#### C 2 {
			A_JumpIf(pressingfire(),"open_fastclose");
		}
		#### C 0 A_CloseCylinder();
		AURG A 0 A_CheckAltAurochs();
		#### A 0 {
			bool alternate=invoker.weaponstatus[0]&AURF_ALTERNATE;
			if(player.getpsprite(PSP_WEAPON).sprite==getspriteindex(alternate?"2URGA0":"AURGA0")){
				setweaponstate("nope");
			}
		}
		#### C 0; 
		#### C 1 offset(0,32);
		#### C 1 offset(-2,40);
		#### C 1 offset(-6,56);
		#### C 1 offset(-7,66);
		#### B 1 offset(12,96);
		#### B 0 A_ClearOverlays();
		#### B 1 offset(10,86);
		#### B 1 offset(8,64);
		#### B 1 offset(4,50);
		#### B 1 offset(2,39);
		goto ready;
	open_fastclose:
		#### B 2;
		#### B 0{
			A_CloseCylinder();
			invoker.wronghand=(Wads.CheckNumForName("id",0)==-1);
			A_CheckAltAurochs();
			A_ClearOverlays();
		}goto ready;
	open_dumpcylinder:
		#### C 4 A_HitExtractor();
		goto readyopen;
	open_dumpcylinder_all:
		#### C 1 offset(0,34);
		#### C 1 offset(0,42);
		#### C 1 offset(0,54);
		#### C 1 offset(0,68);
		TNT1 A 6 A_ExtractAll();
		#### C 1 offset(0,68);
		#### C 1 offset(0,54);
		#### C 1 offset(0,42);
		#### C 1 offset(0,34);
		goto readyopen;

	user1:
	user2:
	swappistols:
		---- A 0 A_SwapHandguns();
		---- A 0{
			bool id=(Wads.CheckNumForName("id",0)!=-1);
			bool offhand=invoker.wronghand;
			bool lefthanded=(id!=offhand);
			if(lefthanded){
				A_Overlay(1025,"raiseleft");
				A_Overlay(1026,"lowerright");
			}else{
				A_Overlay(1025,"raiseright");
				A_Overlay(1026,"lowerleft");
			}
		}
		TNT1 A 5;
		AURG A 0 A_CheckAltAurochs(); //A_CheckPistolHand();
		goto nope;
	lowerright:
		AURG G 0 {
			//console.printf("lowerleft");
			A_JumpIf(invoker.weaponstatus[0]&AURF_COCKED,2);
		}
		AURG A 0;
		#### A 1 offset(-6,38);
		#### A 1 offset(-12,48);
		#### G 1 offset(-20,60);
		#### G 1 offset(-34,76);
		#### G 1 offset(-50,86);
		stop;
	lowerleft:
		ARLG G 0 {
			//console.printf("lowerright");
			A_JumpIf(invoker.weaponstatus[0]&AURF_COCKED,2);
		}
		ARLG A 0;
		#### A 1 offset(6,38);
		#### A 1 offset(12,48);
		#### G 1 offset(20,60);
		#### G 1 offset(34,76);
		#### G 1 offset(50,86);
		stop;
	raiseright:
		ARLG D 1 offset(-50,86);
		#### # 0 {
			//console.printf("raiseleft");
		}
		ARLG D 1 offset(-34,76);
		#### C 0 A_JumpIf(invoker.weaponstatus[0]&AURF_COCKED,2);
		#### A 0;
		#### A 1 offset(-20,60);
		#### A 1 offset(-12,48);
		#### A 1 offset(-6,38);
		stop;
	raiseleft:
		AURG G 1 offset(50,86);
		#### # 0 {
			//console.printf("raiseright");
		}
		AURG G 1 offset(34,76);
		#### A 0 A_JumpIf(invoker.weaponstatus[0]&AURF_COCKED,2);
		#### A 0;
		#### A 1 offset(20,60);
		#### A 1 offset(12,48);
		#### A 1 offset(6,38);
		stop;
	whyareyousmiling:
		#### G 1 offset(0,38);
		#### G 1 offset(0,48);
		#### G 1 offset(0,60);
		TNT1 A 7;
		AURG A 0{
			invoker.wronghand=!invoker.wronghand;
			A_CheckAltAurochs();
		}
		#### G 1 offset(0,60);
		#### G 1 offset(0,48);
		#### G 1 offset(0,38);
		goto nope;
	round1:AVR1 BA 1;wait;
	round1spent:AVE1 BA 1;wait;
	round2:AVR2 BA 1;wait;
	round2spent:AVE2 BA 1;wait;
	teehee:
		AURG ABCDEFG 0;
		AURF A 0;
		ARLG ABCDEFG 0;
		AURR ABCD 0;
		2URG ABCDEFG 0;
		2URF A 0;
		2RLG ABCDEFG 0;
		2URR ABCD 0;
		AURP AB 0;
	
	spawn:
		TNT1 A 1;
		AURP A -1{
			if(
				invoker.weaponstatus[0]&AURF_ALTERNATE
			)invoker.sprite=getspriteindex("2URPA0");
		}
		2URP A -1;
		stop;
	}
}

/*class AurochsSpawn:HD420BoxPickup{
	override void postbeginplay(){
		super.postbeginplay();
		HDF.TransferSpecials(self,spawn("HDAurochs",pos,ALLOW_REPLACE));
	}
}*/
enum AurochsStats{
	//chamber 1 is the shooty one
	
	AURY_CYL1=1,
	AURY_CYL2=2,
	AURY_CYL3=3,
	AURY_CYL4=4,
	AURY_CYL5=5,
	AURY_CYL6=6,
	AURY_OVRCYL=355,


	//odd means spent
	AURY_SIXNINEBORESPENT=1,
	AURY_SIXNINEBORE=2,
	AURY_FOURTWENTYSPENT=3,
	AURY_FOURTWENTY=4,
	
	AURF_RIGHTHANDED=1,
	AURF_COCKED=2,
	AURF_CLOCKWISE = 4,
	AURF_ALTERNATE=8
}

class AurochsSpawn:IdleDummy{
	states{
	spawn:
		TNT1 A 0 nodelay{
			let auu=HDAurochs(spawn("HDAurochs",pos,ALLOW_REPLACE));
			if(!auu)return;
			HDF.TransferSpecials(self,auu);
			if(!random(0,2)){
				auu.weaponstatus[0]|=AURF_ALTERNATE;
			}
			spawn("HD420BoxPickup",pos+(-3,0,0),ALLOW_REPLACE);
		}stop;
	}
}

