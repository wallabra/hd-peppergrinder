// ------------------------------------------------------------
// KAG Greely SuperShorty 12g
// ------------------------------------------------------------
/*  Following the Greely Valley disaster that erased the town and the rest of Iowa
off the map for all but those foolish enough to investigate its aftermath, now
W.I.N.G. operative Kitt Kaneval stumbled from the wreckage with a broken gas tank,
a battered flashlight, and his custom body pump action pistol grip shotgun. Sent
only to clean up the sudden catastrophe, the W.I.N.G. Unit team was surprised to
find the majority of threats beginning to disperse with and a shell shocked Kitt
frantically searching the remains of a destroyed graveyard with a large brass coin
in hand, which had to be wrestled from his grasp before he would co-operate and
calmly recieve aid.

  After stabilizing the sole survivor, the WING Unit began a thourough cleanup
campaign of remaining combatants including the living dead, clowns, elves, and
armed food mascots. Following the quarantining of theremains of Greely Valley,
the Kabocha Arms Group took interest in purchasing the rights to the custom
body model of Agent Kaneval's shotgun, which they promptly added to their
existing line of pump action 12 gauges. */

const HDLD_GREELY="gre";
class HDGreely:HDShotgun{
	default{
		+hdweapon.fitsinbackpack
		weapon.selectionorder 72;
		weapon.slotnumber 3;
		weapon.slotpriority 2.5;
		weapon.bobrangex 0.21;
		weapon.bobrangey 0.86;
		scale 0.6;
		inventory.pickupmessage "You got the KAC Greely Supershorty!";
		hdweapon.barrelsize 20,0.5,2;
		hdweapon.refid HDLD_GREELY;
		tag "Greely 12";
		obituary "%o was carnevil'd by %k";
		
		hdweapon.loadoutcodes "
			\cushell - 0/1, Start with Shells loaded";
	}
	//returns the power of the load just fired
	static double FireShell(actor caller){
		double spread=14.;
		double speedfactor=1.;
		spread=6.6;
		speedfactor=1.03;

		double shotpower=getshotpower();
		spread*=shotpower;
		speedfactor*=shotpower;
		HDBulletActor.FireBullet(caller,"HDB_wad");
		let p=HDBulletActor.FireBullet(caller,"HDB_00",
		spread:spread,speedfactor:speedfactor,amount:10
		);
		distantnoise.make(p,"world/shotgunfar");
		caller.A_StartSound("weapons/greelyshot",CHAN_WEAPON);
		caller.A_StartSound("weapons/greelyblast",9,CHANF_DEFAULT,0.1);
		return shotpower;
	}
	static double FireSlug(actor caller){
		double spread=14.;
		double speedfactor=1.;
		spread=3.5;

		double shotpower=getshotpower();
		spread*=shotpower;
		speedfactor*=shotpower;
		HDBulletActor.FireBullet(caller,"HDB_wad");
		let p=HDBulletActor.FireBullet(caller,"HDB_SLUG",
			spread:spread,speedfactor:1.01
		);
		distantnoise.make(p,"world/shotgunfar");
		caller.A_StartSound("weapons/greelyshot",CHAN_WEAPON);
		caller.A_StartSound("weapons/greelyblast",9,CHANF_DEFAULT,0.1);
		return shotpower;
	}
	action void A_FireGreely(){
		double shotpower=invoker.weaponstatus[GREES_CHAMBER]==4?invoker.FireSlug(self):invoker.FireShell(self);
		A_GunFlash();
		vector2 shotrecoil=(randompick(-1,1),-2.6);
		shotrecoil*=shotpower;
		A_ZoomRecoil(0.855);
		A_MuzzleClimb(0,0,shotrecoil.x,shotrecoil.y,randompick(-4,4)*shotpower,-1.3*shotpower);
		invoker.weaponstatus[GREES_CHAMBER]-=2;
		invoker.shotpower=shotpower;
	}
	override string,double getpickupsprite(){return "GSGPA0",1.;}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			sb.drawimage("SHL1A0",(-47,-10),basestatusbar.DI_SCREEN_CENTER_BOTTOM);
			sb.drawnum(hpl.countinv("HDShellAmmo"),-46,-8,
				basestatusbar.DI_SCREEN_CENTER_BOTTOM
			);
			sb.drawimage("SLG1A0",(-57,-10),basestatusbar.DI_SCREEN_CENTER_BOTTOM);
			sb.drawnum(hpl.countinv("HDSlugAmmo"),-56,-8,
				basestatusbar.DI_SCREEN_CENTER_BOTTOM
			);
		}

		if(hd_debug){ //debug tube checker
			sb.drawnum(hdw.weaponstatus[GREES_CHAMBER],-15,-8,
				basestatusbar.DI_SCREEN_CENTER_BOTTOM);
			sb.drawnum(hdw.weaponstatus[GREES_TB1],-22,-8,
				basestatusbar.DI_SCREEN_CENTER_BOTTOM);
			sb.drawnum(hdw.weaponstatus[GREES_TB2],-27,-8,
				basestatusbar.DI_SCREEN_CENTER_BOTTOM);
			sb.drawnum(hdw.weaponstatus[GREES_TB3],-32,-8,
				basestatusbar.DI_SCREEN_CENTER_BOTTOM);
		}
		int chm=hdw.weaponstatus[GREES_CHAMBER];
		int tb1=hdw.weaponstatus[GREES_TB1];
		int tb2=hdw.weaponstatus[GREES_TB2];
		int tb3=hdw.weaponstatus[GREES_TB3];
		if(chm>0){
			sb.drawrect(-18,-14,2,3);
			if(chm==3){
				sb.drawrect(-24,-14,1,1);sb.drawrect(-22,-14,1,1);sb.drawrect(-20,-14,1,1);
				sb.drawrect(-23,-13,1,1);sb.drawrect(-21,-13,1,1);
				sb.drawrect(-24,-12,1,1);sb.drawrect(-22,-12,1,1);sb.drawrect(-20,-12,1,1);
			}if(chm==4){
				sb.drawrect(-23,-14,4,3);
				sb.drawrect(-24,-13,1,1);
			}
		}
		if(tb1>0){
			sb.drawrect(-18,-10,2,3);
			if(tb1==3){
				sb.drawrect(-23,-10,1,1);sb.drawrect(-21,-10,1,1);sb.drawrect(-19,-10,1,1);
				sb.drawrect(-22,-9,1,1);sb.drawrect(-20,-9,1,1);
				sb.drawrect(-23,-8,1,1);sb.drawrect(-21,-8,1,1);sb.drawrect(-19,-8,1,1);
			}if(tb1==4){
				sb.drawrect(-22,-10,3,3);
				sb.drawrect(-23,-9,1,1);
			}
		}if(tb2>0){
			sb.drawrect(-26,-10,2,3);
			if(tb2==3){
				sb.drawrect(-31,-10,1,1);sb.drawrect(-29,-10,1,1);sb.drawrect(-27,-10,1,1);
				sb.drawrect(-30,-9,1,1);sb.drawrect(-28,-9,1,1);
				sb.drawrect(-31,-8,1,1);sb.drawrect(-29,-8,1,1);sb.drawrect(-27,-8,1,1);
			}if(tb2==4){
				sb.drawrect(-30,-10,3,3);
				sb.drawrect(-31,-9,1,1);
			}
		}if(tb3>0){
			sb.drawrect(-34,-10,2,3);
			if(tb3==3){
				sb.drawrect(-39,-10,1,1);sb.drawrect(-37,-10,1,1);sb.drawrect(-35,-10,1,1);
				sb.drawrect(-38,-9,1,1);sb.drawrect(-36,-9,1,1);
				sb.drawrect(-39,-8,1,1);sb.drawrect(-37,-8,1,1);sb.drawrect(-35,-8,1,1);
			}if(tb3==4){
				sb.drawrect(-38,-10,3,3);
				sb.drawrect(-39,-9,1,1);
				}
			}
		}
//		sb.drawwepnum(hdw.weaponstatus[GREES_TUBE],3,posy:-7);
	
	override string gethelptext(){
		return
		WEPHELP_FIRE.."  Shoot\n"
		..WEPHELP_ALTFIRE.."  Pump\n"
		..WEPHELP_RELOAD.."  Load Shells\n"
		..WEPHELP_ALTRELOAD.."  Load Slugs\n"
		.."When pumped:	"..WEPHELP_ALTRELOAD.."/"..WEPHELP_RELOAD.." to perform a slug/shell changeover\n"
		..WEPHELP_UNLOADUNLOAD
		;
	}
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc,string whichdot
	){
		int cx,cy,cw,ch;
		[cx,cy,cw,ch]=screen.GetClipRect();
		sb.SetClipRect(
			-16+bob.x,-4+bob.y,32,16,
			sb.DI_SCREEN_CENTER
		);
		vector2 bobb=bob*1.1;
		bobb.y=clamp(bobb.y,-8,8);
		sb.drawimage(
			"frntsite",(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			alpha:0.9
		);
		sb.SetClipRect(cx,cy,cw,ch);
		sb.drawimage(
			"backsite",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP
		);
	}
	override double gunmass(){
		return 8 ;
	}
	override double weaponbulk(){
		return 85+(
		weaponstatus[GREES_TB1]>2?1:0
		+weaponstatus[GREES_TB2]>2?1:0
		+weaponstatus[GREES_TB3]>2?1:0
		)*ENC_SHELLLOADED;
	}
	override void DropOneAmmo(int amt){
		if(owner){
			amt=clamp(amt,1,10);
			owner.A_DropInventory("HDSlugAmmo",amt*4);
			owner.A_DropInventory("HDShellAmmo",amt*4);
		}
	}
	override void ForceBasicAmmo(){
		if(weaponstatus[0]&GREEF_SHELLLOADOUT)owner.A_SetInventory("HDShellAmmo",4);
		else owner.A_SetInventory("HDSlugAmmo",4);
	}
	override void DetachFromOwner(){
		if(handshells>0){
			let ammo = (weaponstatus[0] & GREEF_SLUG)? "HDSlugAmmo" : "HDShellAmmo";
			if(owner)owner.A_DropItem(ammo,handshells);
			else A_DropItem(ammo,handshells);
		}
		handshells=0;
		HDWeapon.detachfromowner();
	}
	action void A_SetAltHold(bool which){
		if(which)invoker.weaponstatus[0]|=GREEF_ALTHOLDING;
		else invoker.weaponstatus[0]&=~GREEF_ALTHOLDING;
	}
	action void A_PushGreelyTube(){
		invoker.weaponstatus[GREES_TB1]=invoker.weaponstatus[GREES_TB2];
		invoker.weaponstatus[GREES_TB2]=invoker.weaponstatus[GREES_TB3];
		invoker.weaponstatus[GREES_TB3]=0;
		}
	action void A_Chamber(bool careful=false){
		int chm=invoker.weaponstatus[GREES_CHAMBER];
		invoker.weaponstatus[GREES_CHAMBER]=invoker.weaponstatus[GREES_TB1];
		A_PushGreelyTube();
		vector3 cockdir;double cp=cos(pitch);
		if(careful)cockdir=(-cp,cp,-5);
		else cockdir=(0,-cp*5,sin(pitch)*frandom(4,6));
		cockdir.xy=rotatevector(cockdir.xy,angle);
        bool pocketed=false;
		if(chm>2){
			if(careful&&!A_JumpIfInventory(chm==4?"HDSlugAmmo":"HDShellAmmo",0,"null")){
				HDF.Give(self,chm==4?"HDSlugAmmo":"HDShellAmmo",1);
				pocketed=true;
			}
		}else if(chm>0){	
			cockdir*=frandom(1.,1.3);
		}

		if(
			!pocketed
			&&chm>=1
		){
			vector3 gunofs=HDMath.RotateVec3D((9,-1,-2),angle,pitch);
			actor rrr=null;

			if(chm>2)rrr=spawn(chm==4?"HDFumblingSlug":"HDFumblingShell",(pos.xy,pos.z+height*0.85)+gunofs+viewpos.offset);
			else rrr=spawn(chm==2?"HDSpentSlug":"HDSpentShell",(pos.xy,pos.z+height*0.85)+gunofs+viewpos.offset);

			rrr.target=self;
			rrr.angle=angle;
			rrr.vel=HDMath.RotateVec3D((1,-5,0.2),angle,pitch);
			if(chm==1)rrr.vel*=1.3;
			rrr.vel+=vel;
		}
	}
	action bool A_LoadTubeFromHand(){
		bool slg=invoker.weaponstatus[0]&GREEF_SLUG;
		int hand=invoker.handshells;
		if(
			!hand
			||(
				invoker.weaponstatus[GREES_CHAMBER]>0
				&&invoker.weaponstatus[GREES_TB1]>0
				&&invoker.weaponstatus[GREES_TB2]>0
				&&invoker.weaponstatus[GREES_TB3]>0
			)
		){
			EmptyHand();
			return false;
		}
		invoker.weaponstatus[GREES_TB3]=invoker.weaponstatus[GREES_TB2];
		invoker.weaponstatus[GREES_TB2]=invoker.weaponstatus[GREES_TB1];
		invoker.weaponstatus[GREES_TB1]=slg?4:3;
		invoker.handshells--;
		A_StartSound("weapons/greelyreload",8,CHANF_OVERLAP);
		return true;
	}
	action bool A_GrabShells(int maxhand=3,bool settics=false,bool alwaysone=false){
		bool slg=invoker.weaponstatus[0]&GREEF_SLUG;
		if(maxhand>0)EmptyHand();else maxhand=abs(maxhand);
		int toload=min(
			slg?countinv("HDSlugAmmo"):countinv("HDShellAmmo"),
			1,		//alwaysone?1:(3-invoker.weaponstatus[GREES_TUBE]),
			max(1,health/22),
			maxhand
		);
		if(toload<1)return false;
		invoker.handshells=toload;
		A_TakeInventory(slg?"HDSlugAmmo":"HDShellAmmo",toload,TIF_NOTAKEINFINITE);
		if(settics)A_SetTics(7);
		A_StartSound("weapons/pocket",9);
		A_MuzzleClimb(
			frandom(0.1,0.15),frandom(0.2,0.4),
			frandom(0.2,0.25),frandom(0.3,0.4),
			frandom(0.1,0.35),frandom(0.3,0.4),
			frandom(0.1,0.15),frandom(0.2,0.4)
		);
		return true;
	}
	action void EmptyHand(int amt=-1,bool careful=false){
		if(!amt)return;
		if(amt>0)invoker.handshells=amt;

		while(invoker.handshells>0){
			let slug = invoker.weaponstatus[0] & GREEF_SLUG;
			let ammo = slug? "HDSlugAmmo" : "HDShellAmmo";

			if(careful&&!A_JumpIfInventory(ammo,0,"null")){
				invoker.handshells--;
				HDF.Give(self,ammo,1);
 			}else if(invoker.handshells>=4){
				invoker.handshells-=4;
				A_SpawnItemEx(slug? "HDSlugPickup" : "HDShellPickup",
					cos(pitch)*1,1,height-7-sin(pitch)*1,
					cos(pitch)*cos(angle)*frandom(1,2)+vel.x,
					cos(pitch)*sin(angle)*frandom(1,2)+vel.y,
					-sin(pitch)+vel.z,
					0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
			}else{
				invoker.handshells--;
				A_SpawnItemEx(slug? "HDFumblingSlug" : "HDFumblingShell",
					cos(pitch)*5,1,height-7-sin(pitch)*5,
					cos(pitch)*cos(angle)*frandom(1,4)+vel.x,
					cos(pitch)*sin(angle)*frandom(1,4)+vel.y,
					-sin(pitch)*random(1,4)+vel.z,
					0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
			}
		}
	}
	states{
	select0:
		GSTG A 0;
		goto select0big;
	deselect0:
		GSTG A 0;
		goto deselect0big;
	ready:
		GSTG A 0 A_JumpIf(pressingaltfire(),2);
		GSTG A 0{
			if(!pressingaltfire()){
				if(!pressingfire())A_ClearRefire();
				A_SetAltHold(false);
			}
		}
		GSTG A 1 A_WeaponReady(WRF_ALL);
		GSTG A 0{invoker.weaponstatus[0]&=~GREEF_SLUG;}
		goto readyend;
	hold:
		GSTG A 0{
			bool paf=pressingaltfire();
			if(
				paf&&!(invoker.weaponstatus[0]&GREEF_ALTHOLDING)
			)setweaponstate("chamber");
			else if(!paf)invoker.weaponstatus[0]&=~GREEF_ALTHOLDING;
		}
		GSTG A 1 A_WeaponReady(WRF_NONE);
		GSTG A 0 A_Refire();
		goto ready;
	fire:
		GSTG A 0 A_JumpIf(invoker.weaponstatus[GREES_CHAMBER]>2,"shoot");
		GSTG A 1 A_WeaponReady(WRF_NONE);
		GSTG A 0 A_Refire();
		goto ready;
	shoot:
		GSTG A 2;
		GSTG A 1 offset(0,36) A_FireGreely();
		GSTG E 1;
		GSTG E 0;
		goto ready;
	altfire:
	chamber:
		GSTG A 0 A_JumpIf(invoker.weaponstatus[0]&GREEF_ALTHOLDING,"nope");
		GSTG A 0 A_SetAltHold(true);
		GSTG A 1 A_Overlay(120,"playsgco");
		GSTG AE 1 A_MuzzleClimb(0,frandom(0.6,1.));
		GSTG E 1 A_JumpIf(pressingaltfire(),"longstroke");
		GSTG EA 1 A_MuzzleClimb(0,-frandom(0.6,1.));
		GSTG E 0 A_StartSound("weapons/huntshort",8);
		GSTG E 0 A_Refire("ready");
		goto ready;
	longstroke:
		GSTG F 2 A_MuzzleClimb(frandom(1.,2.));
		GSTG F 0{
			A_Chamber();
			A_MuzzleClimb(-frandom(1.,2.));
		}
	racked:
		GSTG F 1 A_WeaponReady(WRF_NOFIRE);
		GSTG F 0 A_JumpIf(!pressingaltfire(),"unrack");
		GSTG F 0 A_JumpIf(pressingunload(),"rackunload");
		//GSTG F 0 A_JumpIf(invoker.weaponstatus[GREES_CHAMBER],"racked");
		GSTG F 0{
			int rld=0;
			if(pressingreload()){
				rld=1;
			}else if(pressingaltreload()){
				rld=2;
				invoker.weaponstatus[0]|=GREEF_SLUG;
			}
			if(
				rld>0
				&&(countinv("HDSlugAmmo")
					||countinv("HDShellAmmo")
				)
			)setweaponstate("rackreload");
		}
		loop;
	rackreload:
		GSTG F 1 offset(-1,35) A_WeaponBusy(true);
		GSTG F 2 offset(-2,37);
		GSTG F 3 offset(-3,40);
		GSTG F 1 offset(-4,42); //A_GrabShells(1,true,true);
		GSTG F 5 offset(-5,43);
		GSTG F 5 offset(-4,41) A_StartSound("weapons/pocket",9); 
	rackloadone:
		GSTG F 1 offset(-4,42);
		GSTG F 2 offset(-4,41){
			if(PressingAltReload())invoker.weaponstatus[0]|=GREEF_SLUG;
			if(countinv("HDSlugAmmo")>0&&countinv("HDShellAmmo")<1)invoker.weaponstatus[0]|=GREEF_SLUG;
			if(countinv("HDSlugAmmo")<1)invoker.weaponstatus[0]&=~GREEF_SLUG;
			}
		GSTG F 2 offset(-4,40){
			bool slg=invoker.weaponstatus[0]&GREEF_SLUG;
			/*if(invoker.weaponstatus[GREES_CHAMBER]<=0){
				A_TakeInventory(slg?"HDSlugAmmo":"HDShellAmmo",1,TIF_NOTAKEINFINITE);
				invoker.weaponstatus[GREES_CHAMBER]=invoker.weaponstatus[0]&GREEF_SLUG?4:3;
			}*/
			switch(invoker.weaponstatus[GREES_CHAMBER]){
				case 0:
					A_StartSound("weapons/greelyreload",8,CHANF_OVERLAP);
					A_TakeInventory(slg?"HDSlugAmmo":"HDShellAmmo",1,TIF_NOTAKEINFINITE);
					invoker.weaponstatus[GREES_CHAMBER]=invoker.weaponstatus[0]&GREEF_SLUG?4:3;
					break;
				case 3: case 4: 
					if(invoker.weaponstatus[GREES_TB1]>0
					&&invoker.weaponstatus[GREES_TB2]>0
					&&invoker.weaponstatus[GREES_TB3]>0){
						A_Jump(256, 3);
						break;
					}else{
						
						A_StartSound("weapons/greelyreload",8,CHANF_OVERLAP);
						A_TakeInventory(slg?"HDSlugAmmo":"HDShellAmmo",1,TIF_NOTAKEINFINITE);
						invoker.weaponstatus[GREES_TB3]=invoker.weaponstatus[GREES_TB2];
						invoker.weaponstatus[GREES_TB2]=invoker.weaponstatus[GREES_TB1];
						invoker.weaponstatus[GREES_TB1]=invoker.weaponstatus[GREES_CHAMBER]==3?3:4;
						invoker.weaponstatus[GREES_CHAMBER]=invoker.weaponstatus[0]&GREEF_SLUG?4:3;
						break;
					}
				case 1: case 2:
					break;
				default:
					break;
				}
		}
		GSTG F 4 offset(-4,41);
		GSTG F 3 offset(-4,40) A_JumpIf(invoker.handshells>0,"rackloadone");
		goto rackreloadend;
	rackreloadend:
		GSTG F 1 offset(-3,39){invoker.weaponstatus[0]&=~GREEF_SLUG;}
		GSTG F 1 offset(-2,37);
		GSTG F 1 offset(-1,34);
		GSTG F 0 A_WeaponBusy(false);
		goto racked;

	rackunload:
		GSTG F 1 offset(-1,35) A_WeaponBusy(true);
		GSTG F 2 offset(-2,37);
		GSTG F 4 offset(-3,40);
		GSTG F 1 offset(-4,42);
		GSTG F 2 offset(-4,41);
		GSTG F 3 offset(-4,40){
			int chm=invoker.weaponstatus[GREES_CHAMBER];
			invoker.weaponstatus[GREES_CHAMBER]=0;
			if(chm>2){
				A_StartSound("weapons/greelyreload",8);
				if(A_JumpIfInventory(chm==4?"HDSlugAmmo":"HDShellAmmo",0,"null"))
				A_SpawnItemEx(chm==4?"HDFumblingSlug":"HDFumblingShell",
					cos(pitch)*8,0,height-7-sin(pitch)*8,
					vel.x+cos(pitch)*cos(angle-random(86,90))*5,
					vel.y+cos(pitch)*sin(angle-random(86,90))*5,
					vel.z+sin(pitch)*random(4,6),
					0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);else{
					HDF.Give(self,chm==4?"HDSlugAmmo":"HDShellAmmo",1);
					A_StartSound("weapons/pocket",9);
					A_SetTics(5);
				}
			}else if(chm>0)A_SpawnItemEx(chm==2?"HDSpentSlug":"HDSpentShell",
				cos(pitch)*8,0,height-7-sin(pitch)*8,
				vel.x+cos(pitch)*cos(angle-random(86,90))*5,
				vel.y+cos(pitch)*sin(angle-random(86,90))*5,
				vel.z+sin(pitch)*random(4,6),
				0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
			);
			//if(chm)A_StartSound("weapons/greelyreload",8,CHANF_OVERLAP);
		}
		GSTG F 5 offset(-4,41);
		GSTG F 4 offset(-4,40) A_JumpIf(invoker.handshells>0,"rackloadone");
		goto rackreloadend;

	unrack:
		GSTG F 0 A_Overlay(120,"playsgco2");
		GSTG E 1 A_JumpIf(!pressingfire(),1);
		GSTG EA 2{
			if(pressingfire())A_SetTics(1);
			A_MuzzleClimb(0,-frandom(0.6,1.));
		}
		GSTG A 0 A_ClearRefire();
		goto ready;
	playsgco:
		TNT1 A 8 A_StartSound("weapons/greelyrackup",8);
		TNT1 A 0 A_StopSound(8);
		stop;
	playsgco2:
		TNT1 A 8 A_StartSound("weapons/greelyrackdown",8);
		TNT1 A 0 A_StopSound(8);
		stop;
	flash:
		GSTF B 1 bright{
			A_Light2();
			HDFlashAlpha(-32);
		}
		TNT1 A 1 A_ZoomRecoil(0.9);
		TNT1 A 0 A_Light0();
		TNT1 A 0 A_AlertMonsters();
		stop;
	altreload:
		GSTG A 0{invoker.weaponstatus[0]|=GREEF_SLUG;}
	reload:
	startreload:
		GSTG A 0{
			int ppp=countinv("HDShellAmmo");
			int sss=countinv("HDSlugAmmo");
			if(ppp<1&&sss<1)setweaponstate("nope");
			}
		GSTG A 1 A_JumpIf(invoker.weaponstatus[GREES_TB3]>0,"nope");
		GSTG AB 3 A_MuzzleClimb(frandom(.6,.7),-frandom(.6,.7));
	reloadstarthand:
		GSTG C 1 offset(0,36);
		GSTG C 1 offset(0,38);
		GSTG C 1 offset(0,36);
		GSTG C 2 offset(0,34);
		GSTG C 2 offset(0,36);
		GSTG C 2 offset(0,40){
			if(PressingAltReload())invoker.weaponstatus[0]|=GREEF_SLUG;
			if(countinv("HDSlugAmmo")>0&&countinv("HDShellAmmo")<1)invoker.weaponstatus[0]|=GREEF_SLUG;
			if(countinv("HDSlugAmmo")<1)invoker.weaponstatus[0]&=~GREEF_SLUG;
			}
	reloadpocket:
		GSTG C 1 offset(0,39)A_GrabShells(3,false);
		GSTG C 2 offset(0,40) A_JumpIf(health>40,1);
		GSTG C 1 offset(0,40) A_StartSound("weapons/pocket",9);
		GSTG C 4 offset(0,42) A_StartSound("weapons/pocket",9);
		GSTG C 2 offset(0,41) A_StartSound("weapons/pocket",9);
		GSTG C 2 offset(0,40);
		goto reloadashell;
	reloadashell:
		GSTG C 1 offset(0,36);
		GSTG C 2 offset(0,34)A_LoadTubeFromHand();
		GSTG CCCCCC 1 offset(0,33){
			if(
				PressingReload()
				||PressingAltReload()
				||PressingUnload()
				||PressingFire()
				||PressingAltfire()
				||PressingZoom()
				||PressingFiremode()
			)invoker.weaponstatus[0]|=GREEF_HOLDING;
			else invoker.weaponstatus[0]&=~GREEF_HOLDING;
			invoker.weaponstatus[0]&=~GREEF_SLUG;
			if(
				invoker.weaponstatus[GREES_TB3]>0
				||(
					invoker.handshells<1
					&&!countinv("HDShellAmmo")
					&&!countinv("HDSlugAmmo")
				)
			)setweaponstate("reloadend");
			else if(
				!pressingaltreload()
				&&!pressingreload()
			)setweaponstate("reloadend");
			else if(invoker.handshells<1)setweaponstate("reloadstarthand");
		}goto reloadashell;
	reloadend:
		GSTG C 3 offset(0,34) A_StartSound("weapons/huntopen",8, CHANF_OVERLAP);
		GSTG C 1 offset(0,36) EmptyHand(careful:true);
		GSTG C 1 offset(0,34){invoker.weaponstatus[0]&=~GREEF_SLUG;}
		GSTG CBA 2;
		GSTG A 0 A_JumpIf(invoker.weaponstatus[0]&GREEF_HOLDING,"nope");
		goto ready;

	unload:
		GSTG A 1{
			if(
				invoker.weaponstatus[GREES_CHAMBER]<1
				&&invoker.weaponstatus[GREES_TB1]<1
			)setweaponstate("nope");
		}
		GSTG BC 4 A_MuzzleClimb(frandom(1.2,2.4),-frandom(1.2,2.4));
		GSTG C 1 offset(0,34);
		GSTG C 1 offset(0,36) A_StartSound("weapons/huntopen",8);
		GSTG C 1 offset(0,38);
		GSTG C 4 offset(0,36){
			A_MuzzleClimb(-frandom(1.2,2.4),frandom(1.2,2.4));
			if(invoker.weaponstatus[GREES_CHAMBER]<1){
				setweaponstate("unloadtube");
			}else A_StartSound("weapons/huntrack",8,CHANF_OVERLAP);
		}
		GSTG D 8 offset(0,34){
			A_MuzzleClimb(-frandom(1.2,2.4),frandom(1.2,2.4));
			int chm=invoker.weaponstatus[GREES_CHAMBER];
			invoker.weaponstatus[GREES_CHAMBER]=0;
			if(chm>2){
				A_StartSound("weapons/greelyreload",8);
				if(A_JumpIfInventory(chm==4?"HDSlugAmmo":"HDShellAmmo",0,"null"))
				A_SpawnItemEx(chm==4?"HDFumblingSlug":"HDFumblingShell",
					cos(pitch)*8,0,height-7-sin(pitch)*8,
					vel.x+cos(pitch)*cos(angle-random(86,90))*5,
					vel.y+cos(pitch)*sin(angle-random(86,90))*5,
					vel.z+sin(pitch)*random(4,6),
					0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);else{
					HDF.Give(self,chm==4?"HDSlugAmmo":"HDShellAmmo",1);
					A_StartSound("weapons/pocket",9);
					A_SetTics(5);
				}
			}else if(chm>0)A_SpawnItemEx(chm==2?"HDSpentSlug":"HDSpentShell",
				cos(pitch)*8,0,height-7-sin(pitch)*8,
				vel.x+cos(pitch)*cos(angle-random(86,90))*5,
				vel.y+cos(pitch)*sin(angle-random(86,90))*5,
				vel.z+sin(pitch)*random(4,6),
				0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
			);
		}
		GSTG C 0 A_JumpIf(!pressingunload(),"reloadend");
		GSTG C 4 offset(0,40);
	unloadtube:
		GSTG C 6 offset(0,40) EmptyHand(careful:true);
	unloadloop:
		GSTG C 8 offset(1,41){
			if(invoker.weaponstatus[GREES_TB1]<1)setweaponstate("reloadend");
			else if(invoker.handshells>=3)setweaponstate("unloadloopend");
			else {
				for (int i = GREES_TB1; i <= GREES_TB3; i--) {
					if (invoker.weaponstatus[i] < 1) continue;

					if (invoker.weaponstatus[i] == 4) invoker.weaponstatus[0] |= GREEF_SLUG;
					invoker.weaponstatus[i] = 0;
					invoker.handshells = 1;
					A_PushGreelyTube();

					break;
				}
			}
/*			if(invoker.weaponstatus[GREES_TB1]==4)invoker.weaponstatus[0]|=GREEF_SLUG;
			invoker.weaponstatus[GREES_TB1]=invoker.weaponstatus[GREES_TB2];
			invoker.weaponstatus[GREES_TB2]=invoker.weaponstatus[GREES_TB3];
			invoker.weaponstatus[GREES_TB3]=0;
*/		}
		GSTG C 4 offset(0,40){
			A_StartSound("weapons/greelyreload",8);
			EmptyHand(careful: true);
			invoker.weaponstatus[0]&=~GREEF_SLUG;
			}
		loop;
	unloadloopend:
		GSTG C 6 offset(1,41);
		GSTG C 3 offset(1,42){
			A_StartSound("weapons/pocket",9);
			A_SetTics(8);
		}
		GSTG C 6 {
			invoker.weaponstatus[0]&=~GREEF_SLUG;
			A_Jumpif(!pressingunload(),"reloadend");
			}
		goto unloadloop;
	spawn:
		GSGP A -1 nodelay{
		}
	}
	override void InitializeWepStats(bool idfa){
		if(weaponstatus[0]&GREEF_SHELLLOADOUT){
			weaponstatus[GREES_CHAMBER]=3;
			weaponstatus[GREES_TB1]=3;
			weaponstatus[GREES_TB2]=3;
			weaponstatus[GREES_TB3]=3;
			handshells=0;
		}else{
		weaponstatus[GREES_CHAMBER]=4;
		weaponstatus[GREES_TB1]=4;
		weaponstatus[GREES_TB2]=4;
		weaponstatus[GREES_TB3]=4;
		handshells=0;
		}
	}
	override void loadoutconfigure(string input){
		int shell=min(getloadoutvar(input,"shell",1),1); //Start with Shells
		if(shell>=0){
			weaponstatus[0]|=GREEF_SHELLLOADOUT;
			weaponstatus[GREES_CHAMBER]=3;
			weaponstatus[GREES_TB1]=3;
			weaponstatus[GREES_TB2]=3;
			weaponstatus[GREES_TB3]=3;
		}
	}
}
enum greelystatus{
	GREES_TB1=1, //mouth of tube
	GREES_TB2=2,
	GREES_TB3=3, 
	GREES_CHAMBER=4, //0 empty, 1 spent shl, 2 spent slg, 3 shell, 4 slug
	GREES_HAND=5,

	GREEF_SLUG=1,
	GREEF_UNLOADONLY=2,
	GREEF_SHELLLOADOUT=4, //Refreshes with shells at the range ammobox
	GREEF_ALTHOLDING=8,
	GREEF_HOLDING=16
};

class GreelySpawn:IdleDummy{
	states{
	spawn:
		TNT1 A 0 nodelay{
			let zzz=HDGreely(spawn("HDGreely",pos,ALLOW_REPLACE));
			HDF.TransferSpecials(self, zzz);
			spawn("SlugBoxPickup",pos+(3,0,0),ALLOW_REPLACE);
		}stop;
	}
}
