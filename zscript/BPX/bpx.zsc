// ------------------------------------------------------------
// Pistol Caliber Carbine
// (LIG Mauer BPX)
// ------------------------------------------------------------
const HDLD_BPX="bpx";

class HDBPX:HDWeapon{
	default{
		//$Category "Weapons/Hideous Destructor"
		//$Title "Pistol Caliber Carbine"
		//$Sprite "BPXPA0"
		
		obituary "%o was soaked by %k's pea stream.";
		weapon.selectionorder 24;
		weapon.slotnumber 2;
		weapon.slotpriority 1;
		weapon.kickback 30;
		weapon.bobrangex 0.21;
		weapon.bobrangey 0.9;
		scale 0.55;
		inventory.pickupmessage "You got the pistol caliber carbine!";
		hdweapon.barrelsize 33,0.5,1;
		hdweapon.refid HDLD_BPX;
		tag "BPX Pistol Caliber Carbine";
		inventory.icon "BPXPA0";
		hdweapon.loadoutcodes "
			\cureflex - 0/1, LIG Mauer Roomba reflex sight
			\cuscope - 0/1, LIG Mauer Dragon7 scope
			\cubulletdrop - 0-1200, amount of compensation for bullet drop
			\cuzoom - 6-70, 10x the resulting FOV in degrees
		";
	}
	override bool AddSpareWeapon(actor newowner){return AddSpareWeaponRegular(newowner);}
	override hdweapon GetSpareWeapon(actor newowner,bool reverse,bool doselect){return GetSpareWeaponRegular(newowner,reverse,doselect);}
	
	/*override double gunmass(){
		return 8;//+(weaponstatus[BPXS_MAG]<0)?-0.5:(weaponstatus[BPXS_MAG]*0.02);
	}*/
	
	override double gunmass(){
		int mgg=weaponstatus[BPXS_MAG];
		return 5+(mgg<0?0:0.5*(mgg+1));
	}
	override void postbeginplay(){
		super.postbeginplay();
		weaponspecial=1337;
	}
	
	override double weaponbulk(){
		int mg=weaponstatus[BPXS_MAG];
		int scope=weaponstatus[BPXF_SIGHTINGSYSTEM];
		if(mg<0)return (scope==2?101:98);
		else return (scope==2?101:98+ENC_9MAG_LOADED)+mg*ENC_9_LOADED;
	}
	override void failedpickupunload(){
		failedpickupunloadmag(BPXS_MAG,"HD9mMag15");
	}
	override void DropOneAmmo(int amt){
		if(owner){
			amt=clamp(amt,1,10);
			if(owner.countinv("HDPistolAmmo"))owner.A_DropInventory("HDPistolAmmo",amt*15);
			else owner.A_DropInventory("HD9mMag15",amt);
		}
	}
	override void ForceBasicAmmo(){
		owner.A_TakeInventory("HDPistolAmmo");
		ForceOneBasicAmmo("HD9mMag15");
	}
	override string,double getpickupsprite(){
		string letter;
		int sightsystem = weaponstatus[BPXF_SIGHTINGSYSTEM];
		/*return ((wep0&BPXF_SIGHTINGSYSTEM)?"BPXS":"BPXP")
			..((GetSpareWeaponValue(BPXS_MAG,usespare)<0)?"B":"A").."0",1.;*/
		switch (sightsystem){
			case 0: letter = "P"; break;
			case 1: letter = "S"; break;
			case 2: letter = "Z"; break;
			default: letter = "P"; break;
		}
		return "BPX"..letter..(weaponstatus[BPXS_MAG]<0?"B":"A").."0",1.;
	}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			int nextmagloaded=sb.GetNextLoadMag(hdmagammo(hpl.findinventory("HD9mMag15")));
			if(nextmagloaded>=15){
				sb.drawimage("CLP2NORM",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM,scale:(1,1));
			}else if(nextmagloaded<1){
				sb.drawimage("CLP2EMPTY",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM,alpha:nextmagloaded?0.6:1.,scale:(1,1));
			}else sb.drawbar(
				"CLP2NORM","CLP2GREY",
				nextmagloaded,15,
				(-46,-3),-1,
				sb.SHADER_VERT,sb.DI_SCREEN_CENTER_BOTTOM
			);
			sb.drawnum(hpl.countinv("HD9mMag15"),-43,-8,sb.DI_SCREEN_CENTER_BOTTOM);
		}
		sb.drawwepnum(hdw.weaponstatus[BPXS_MAG],15);
		if(hdw.weaponstatus[BPXS_CHAMBER]==2)sb.drawrect(-19,-11,3,1);
		if(hdw.weaponstatus[BPXF_SIGHTINGSYSTEM]>=2){
			sb.drawstring(
				sb.mAmountFont,string.format("%.1f",hdw.weaponstatus[BPXS_ZOOM]*0.1),
				(-36,-18),sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_TEXT_ALIGN_RIGHT,Font.CR_DARKGRAY
			);
			sb.drawstring(
				sb.mAmountFont,string.format("%i",hdw.weaponstatus[BPXS_DROPADJUST]),
				(-16,-18),sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_TEXT_ALIGN_RIGHT,Font.CR_WHITE
			);
		}
	}
	override string gethelptext(){
		int sightsystem = weaponstatus[BPXF_SIGHTINGSYSTEM];
		return
		WEPHELP_FIRESHOOT
		..WEPHELP_RELOAD.."  Reload mag\n"
		..WEPHELP_USE.."+"..WEPHELP_RELOAD.."  Reload chamber\n"
		..WEPHELP_MAGMANAGER
		..WEPHELP_UNLOADUNLOAD
		..(sightsystem>=2?(WEPHELP_ZOOM.."+"..WEPHELP_FIREMODE.."+"..WEPHELP_UPDOWN.."  Zoom\n"):"")
		..(sightsystem>=2?(WEPHELP_ZOOM.."+"..WEPHELP_USE.."  Bullet drop\n"):"")
		;
	}
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc
	){
        vector2 bobb=bob*1.18;
		if(weaponstatus[BPXF_SIGHTINGSYSTEM]==1){
			double dotoff=max(abs(bob.x),abs(bob.y));
			if(dotoff<30){
				string whichdot=sb.ChooseReflexReticle(hdw.weaponstatus[BPXS_DOT]);
				sb.drawimage(
					whichdot,(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
					alpha:0.8-dotoff*0.01,scale:(0.80,0.80),
					col:0xFF000000|sb.crosshaircolor.GetInt()
				);
			}
			sb.drawimage(
				"xhbpx",(0,1)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,scale:(1.35,1.35)
			);
		}else if (weaponstatus[BPXF_SIGHTINGSYSTEM]==2){
			int cx,cy,cw,ch;
			[cx,cy,cw,ch]=Screen.GetClipRect();
			sb.SetClipRect(
				-16+bob.x,-4+bob.y,32,16,
				sb.DI_SCREEN_CENTER
			);
			bobb.y=clamp(bobb.y,-8,8);
			sb.SetClipRect(cx,cy,cw,ch);
			sb.drawimage(
				"scslsite",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
				scale:(1,0.8)
			);
		}else{
			int cx,cy,cw,ch;
			[cx,cy,cw,ch]=screen.GetClipRect();
			sb.SetClipRect(
				-16+bob.x,-4+bob.y,32,16,
				sb.DI_SCREEN_CENTER
			);
			bobb.y=clamp(bobb.y,-8,8);
			sb.drawimage(
				"smgfrntsit",(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,scale:(1.2,1.07)
			);
			sb.SetClipRect(cx,cy,cw,ch);
			sb.drawimage(
				"bpxsite",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
				alpha:0.9,scale:(2,1.06)
			);
		}
		if(weaponstatus[BPXF_SIGHTINGSYSTEM]==2){
			if(scopeview){
				int scaledyoffset=60;
				int scaledwidth=72;
				double degree=hdw.weaponstatus[BPXS_ZOOM]*0.1;
				double deg=1/degree;
				int cx,cy,cw,ch;
				[cx,cy,cw,ch]=screen.GetClipRect();
				sb.SetClipRect(
					-36+bob.x,24+bob.y,scaledwidth,scaledwidth,
					sb.DI_SCREEN_CENTER
				);
				sb.fill(color(255,0,0,0),
					bob.x-36,scaledyoffset+bob.y-36,
					72,72,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER
				);

				texman.setcameratotexture(hpc,"HDXCAM_LIB",degree);
				let cam  = texman.CheckForTexture("HDXCAM_LIB",TexMan.Type_Any);
				let reticle = texman.CheckForTexture("reticle1",TexMan.Type_Any);
						vector2 frontoffs=(0,scaledyoffset)+bob*2;

						double camSize = texman.GetSize(cam);
						sb.DrawCircle(cam,frontoffs,.08825,usePixelRatio:true);

						let reticleScale = camSize / texman.GetSize(reticle);
			   
				sb.DrawCircle(reticle,(0,scaledyoffset)+bob,.403*reticleScale, uvScale: .52);
					

					//let holeScale    = camSize / texman.GetSize(hole);
					//let hole    = texman.CheckForTexture("scophole",TexMan.Type_Any);
					//sb.DrawCircle(hole, (0, scaledyoffset) + bob, .5 * holeScale, bob * 5, 1.5);
				
				screen.SetClipRect(cx,cy,cw,ch);

					sb.drawimage(
						"libscope",(0,scaledyoffset)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER
					);
					sb.drawstring(
						sb.mAmountFont,string.format("%.1f",degree),
						(6+bob.x,95+bob.y),sb.DI_SCREEN_CENTER|sb.DI_TEXT_ALIGN_RIGHT,
						Font.CR_BLACK
					);
					sb.drawstring(
						sb.mAmountFont,string.format("%i",hdw.weaponstatus[BPXS_DROPADJUST]),
						(6+bob.x,17+bob.y),sb.DI_SCREEN_CENTER|sb.DI_TEXT_ALIGN_RIGHT,
						Font.CR_BLACK
					);
			}
		}
    }
	//override void SetReflexReticle(int which){weaponstatus[BPXS_DOT]=which;}
	/*action void A_CheckReflexSight(){
		if(
			invoker.weaponstatus[BPXF_SIGHTINGSYSTEM]==1
		)Player.GetPSprite(PSP_WEAPON).sprite=getspriteindex("BPSGA0");
		else Player.GetPSprite(PSP_WEAPON).sprite=getspriteindex("BPXGA0");
	}*/
	
	//shamelessly yanked from the lotus
	action void A_CheckBPXSight(){
		int sightsystem=invoker.weaponstatus[BPXF_SIGHTINGSYSTEM];
		switch(sightsystem){
			case 0:
				player.getpsprite(PSP_WEAPON).sprite=getspriteindex("BPXGA0");
				break;
			case 1:
				player.getpsprite(PSP_WEAPON).sprite=getspriteindex("BPSGA0");
				break;
			case 2:
				player.getpsprite(PSP_WEAPON).sprite=getspriteindex("BPZGA0");
				break;
			default:
				break;
		}
	}
	
	action void A_CheckBPXReloadSight(){
		int sightsystem=invoker.weaponstatus[BPXF_SIGHTINGSYSTEM];
		switch(sightsystem){
			case 0:
				player.getpsprite(PSP_WEAPON).sprite=getspriteindex("BPXRA0");
				break;
			case 1:
				player.getpsprite(PSP_WEAPON).sprite=getspriteindex("BPSRA0");
				break;
			case 2:
				player.getpsprite(PSP_WEAPON).sprite=getspriteindex("BPZRA0");
				break;
			default:
				break;
		}
	}
	
	states{
	select0:
		BPXG A 0 A_CheckDefaultReflexReticle(BPXS_DOT);
		BPXG A 0 A_CheckBPXSight();
		goto select0small;
	deselect0:
		BPXG A 0 A_CheckBPXSight();
		goto deselect0small;
		BPXG AB 0;
		BPSG AB 0;
		BPZG AB 0;

	ready:
		BPXG A 0 A_CheckBPXSight();
		#### A 1{
			//console.printf("sightingsystem: "..invoker.weaponstatus[BPXF_SIGHTINGSYSTEM]);
			A_SetCrosshair(21);
			//if(invoker.weaponstatus[BPXF_SIGHTINGSYSTEM]==2){
				if(pressingzoom()&&invoker.weaponstatus[BPXF_SIGHTINGSYSTEM]==2){
					if(player.cmd.buttons&BT_USE){
						A_ZoomAdjust(BPXS_DROPADJUST,0,1200,BT_USE);
					}else A_ZoomAdjust(BPXS_ZOOM,6,70);
					A_WeaponReady(WRF_NONE);
				}else A_WeaponReady(WRF_ALL);
		}
		goto readyend;
	user3:
		---- A 0 A_MagManager("HD9mMag15");
		goto ready;
	altfire:
		goto chamber_manual;
	althold:
		goto nope;
	hold:
		goto nope;
	user2:
	firemode:
		goto nope;
	fire:
		#### A 0 A_JumpIf(pressingfire(),"fire2");
		loop;
	fire2:
		#### B 1{
			if(invoker.weaponstatus[BPXS_CHAMBER]==2)A_GunFlash();
			else setweaponstate("chamber_manual");
		}
		#### A 1;
		#### A 0{
			if(invoker.weaponstatus[BPXS_CHAMBER]==1){
				A_EjectCasing("HDSpent9mm",
					frandom(-1,2),
					(frandom(0.2,0.3),-frandom(7,7.5),frandom(0,0.2)),
					(0,0,-2)
				);
				invoker.weaponstatus[BPXS_CHAMBER]=0;
			}
			if(invoker.weaponstatus[BPXS_MAG]>0){
				invoker.weaponstatus[BPXS_MAG]--;
				invoker.weaponstatus[BPXS_CHAMBER]=2;
			}
		}
		#### A 0 A_ReFire();
		goto ready;
	flash:
		#### B 0{
			let bbb=HDBulletActor.FireBullet(self,"HDB_9",aimoffy:(-HDCONST_GRAVITY/1000.)*invoker.weaponstatus[BPXS_DROPADJUST],speedfactor:1.30);
			if(
				frandom(16,ceilingz-floorz)<bbb.speed*0.1
			)A_AlertMonsters();

			A_ZoomRecoil(0.999);
			A_StartSound("weapons/bpx",CHAN_WEAPON);
			invoker.weaponstatus[BPXS_CHAMBER]=1;
		}
		BPXF A 1 bright{
			HDFlashAlpha(-200);
			A_Light1();
		}
		TNT1 A 0 A_MuzzleClimb(-frandom(0.15,0.22),-frandom(0.25,0.33),-frandom(0.15,0.22),-frandom(0.25,0.33));
		goto lightdone;

	unloadchamber:
		//#### A 0 A_JumpIf(invoker.weaponstatus[BPXF_SIGHTINGSYSTEM]==1, "unloadchambersight");
		//BPXG A 0 A_CheckBPXReloadSight();
		#### D 2 A_JumpIf(invoker.weaponstatus[BPXS_CHAMBER]<1,"nope");
		#### E 2 A_PlaySound("weapons/bpxboltback",CHAN_6,0.5);
		#### F 3 {
			class<actor>which=invoker.weaponstatus[BPXS_CHAMBER]>1?"HDPistolAmmo":"HDSpent9mm";
			invoker.weaponstatus[BPXS_CHAMBER]=0;
			A_EjectCasing(which,
					frandom(-1,2),
					(frandom(0.2,0.3),-frandom(7,7.5),frandom(0,0.2)),
					(0,0,-2)
				);

		}
		#### E 2 A_PlaySound("weapons/bpxboltforward",CHAN_6,0.5);
		#### D 2;
		goto readyend;
	loadchamber:
		---- A 0 A_JumpIf(invoker.weaponstatus[BPXS_CHAMBER]>0,"nope");
		---- A 0 A_JumpIf(!countinv("HDPistolAmmo"),"nope");
		---- A 1 offset(0,34) A_StartSound("weapons/pocket",9);
		---- A 1 offset(2,36);
		---- A 1 offset(2,44);
		#### B 1 offset(5,58);
		#### B 2 offset(7,70);
		#### B 6 offset(8,80);
		#### A 10 offset(8,87){
			if(countinv("HDPistolAmmo")){
				A_TakeInventory("HDPistolAmmo",1,TIF_NOTAKEINFINITE);
				invoker.weaponstatus[BPXS_CHAMBER]=2;
				A_StartSound("weapons/smgchamber",8);
			}else A_SetTics(4);
		}
		#### A 3 offset(9,76);
		---- A 2 offset(5,70);
		---- A 1 offset(5,64);
		---- A 1 offset(5,52);
		---- A 1 offset(5,42);
		---- A 1 offset(2,36);
		---- A 2 offset(0,34);
		goto nope;
	user4:
	unload:
		BPXR A 0 A_CheckBPXReloadSight();
		#### A 0{
			//console.printf("\cjsightingsystem: "..invoker.weaponstatus[BPXF_SIGHTINGSYSTEM]);
			invoker.weaponstatus[0]|=BPXF_JUSTUNLOAD;
			if(
				invoker.weaponstatus[BPXS_MAG]>=0
			)setweaponstate("unmag");
			else if(invoker.weaponstatus[BPXS_CHAMBER]>0)setweaponstate("unloadchamber");
		}goto nope;
		BPSR A 0;
		BPZR A 0;
	reload:
		BPXR A 0 A_CheckBPXReloadSight();
		#### A 0{
			invoker.weaponstatus[0]&=~BPXF_JUSTUNLOAD;
			bool nomags=HDMagAmmo.NothingLoaded(self,"HD9mMag15");
			if(invoker.weaponstatus[BPXS_MAG]>=15)setweaponstate("nope");
			else if(
				invoker.weaponstatus[BPXS_MAG]<1
				&&(
					pressinguse()
					||nomags
				)
			){
				if(
					countinv("HDPistolAmmo")
				)setweaponstate("loadchamber");
				else setweaponstate("nope");
			}else if(nomags)setweaponstate("nope");
			//console.printf("sightingsystem: "..invoker.weaponstatus[BPXF_SIGHTINGSYSTEM]);
		}goto unmag;
	//there is a better solution to this and i will ask marisa at some point if she can help
	unmag:
		#### A 0;
		#### A 1 offset(0,34) A_SetCrosshair(21);
		#### A 1 offset(5,38);
		#### A 1 offset(10,42);
		#### B 2 offset(20,46) A_PlaySound("weapons/bpxmagout",CHAN_WEAPON);
		#### C 4 offset(30,52){
			A_MuzzleClimb(0.3,0.4);
		}
		#### C 0{
			int magamt=invoker.weaponstatus[BPXS_MAG];
			if(magamt<0){
				setweaponstate("magout");
				return;
			}
			invoker.weaponstatus[BPXS_MAG]=-1;
			if(
				(!PressingUnload()&&!PressingReload())
				||A_JumpIfInventory("HD9mMag15",0,"null")
			){
				HDMagAmmo.SpawnMag(self,"HD9mMag15",magamt);
				setweaponstate("magout");
			}else{
				HDMagAmmo.GiveMag(self,"HD9mMag15",magamt);
				A_StartSound("weapons/pocket",9);
				setweaponstate("pocketmag");
			}
		}
	pocketmag:
		#### CC 7 offset(34,54) A_MuzzleClimb(frandom(0.2,-0.8),frandom(-0.2,0.4));
	magout:
		#### C 0{
			if(invoker.weaponstatus[0]&BPXF_JUSTUNLOAD)setweaponstate("reloadend");
			else setweaponstate("loadmag");
		}

	loadmag:
		#### C 0 A_StartSound("weapons/pocket",9);
		#### C 6 offset(34,54) A_MuzzleClimb(frandom(0.2,-0.8),frandom(-0.2,0.4));
		#### C 7 offset(34,52) A_MuzzleClimb(frandom(0.2,-0.8),frandom(-0.2,0.4));
		#### C 10 offset(32,50);
		#### C 3 offset(32,49){
			let mmm=hdmagammo(findinventory("HD9mMag15"));
			if(mmm){
				invoker.weaponstatus[BPXS_MAG]=mmm.TakeMag(true);
				A_StartSound("weapons/bpxmagin",8,CHANF_OVERLAP);
			}
			if(
				invoker.weaponstatus[BPXS_MAG]<1
				||invoker.weaponstatus[BPXS_CHAMBER]>0
			)setweaponstate("reloadend");
		}
		goto reloadend;

	reloadend:
		#### B 3 offset(30,52);
		#### B 2 offset(20,46);
		#### A 1 offset(10,42);
		#### A 1 offset(5,38);
		#### A 1 offset(0,34);
		goto chamber_manual;

	chamber_manual:
		BPXR A 0 A_CheckBPXReloadSight();
		#### A 0 A_JumpIf(
			invoker.weaponstatus[BPXS_MAG]<1
			||invoker.weaponstatus[BPXS_CHAMBER]==2
		,"nope");
		#### A 2;
		#### A 2;
		#### D 2{
			invoker.weaponstatus[BPXS_MAG]--;
			invoker.weaponstatus[BPXS_CHAMBER]=2;
		}
		#### E 3;
		#### F 3 A_PlaySound("weapons/bpxboltback",CHAN_WEAPON);
		#### F 4;
		#### D 3 A_PlaySound("weapons/bpxboltforward",CHAN_WEAPON);
		goto readyend;

	spawn:
		TNT1 A 1;
		BPXP A -1{
			if(invoker.weaponstatus[BPXS_MAG]<0)frame=1;
			if(invoker.weaponstatus[BPXF_SIGHTINGSYSTEM]==1){
				invoker.sprite=getspriteindex("BPXS0");
			}else if(invoker.weaponstatus[BPXF_SIGHTINGSYSTEM]==2){
				invoker.sprite=getspriteindex("BPXZ0");
			}
		}
		BPXS C -1;
		BPXZ C -1;
		stop;
	}
	override void initializewepstats(bool idfa){
		weaponstatus[BPXS_MAG]=15;
		weaponstatus[BPXS_CHAMBER]=2;
		//if(!(weaponstatus[BPXF_SIGHTINGSYSTEM]>0))weaponstatus[BPXF_SIGHTINGSYSTEM]=0;
		if(!idfa&&!owner){
			weaponstatus[BPXS_DOT]=1;
			weaponstatus[BPXS_ZOOM]=30;
			weaponstatus[BPXS_DROPADJUST]=500; //zeroed for 150m
		}
	}
	override void loadoutconfigure(string input){
		
		int xhdot=getloadoutvar(input,"dot",3);
		if(xhdot>=0)weaponstatus[BPXS_DOT]=xhdot;
		int reflexsight=getloadoutvar(input,"reflex",1);
		if(reflexsight>=0){
			weaponstatus[BPXF_SIGHTINGSYSTEM]=1;
		}
		int scope=getloadoutvar(input,"scope",1);
		if(scope>=0){
			weaponstatus[BPXF_SIGHTINGSYSTEM]=2;
		}
	}
}

/*enum stupit{
	BPXF_SIGHTINGSYSTEM
}*/

class BPXSpawn:IdleDummy{
	states{
	spawn:
		TNT1 A 0 nodelay{
			let zzz=HDBPX(spawn("HDBPX",pos,ALLOW_REPLACE));
			if(!zzz)return;
			HDF.TransferSpecials(self, zzz);
			if(!random(0,2)){
				if(!random(0,2))zzz.weaponstatus[BPXF_SIGHTINGSYSTEM]=1;
				else zzz.weaponstatus[BPXF_SIGHTINGSYSTEM]=2;
			}
			spawn("HD9mMag15",pos+(3,0,0),ALLOW_REPLACE);
		}stop;
	}
}

enum bpxstats{
	BPXF_JUSTUNLOAD=1,
	
	BPXS_MAG=1,
	BPXS_CHAMBER=2, 
	BPXS_DOT=3,
	BPXS_ZOOM=4,
	BPXS_DROPADJUST=5,
	BPXF_SIGHTINGSYSTEM=6
}

