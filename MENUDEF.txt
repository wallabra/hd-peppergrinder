AddOptionMenu "OptionsMenu"
{
	Submenu "Peppergrinder", "PeppergrinderMenu"
}

OptionMenu "PeppergrinderMenu"
{
	Title "Peppergrinder Options"
	
	// [Ace] You can have up to 32 of these. On a single CVar, that is.
	// Would have been 64 if GZDoom integers were 8-bytes but oh well.
	StaticText "----- Weapon Spawns -----", "Teal"
	PepperSpawnOption "Otis-5", "pepper_weaponspawns_1", "YesNo", 0
	PepperSpawnOption "Trog", "pepper_weaponspawns_1", "YesNo", 1
	PepperSpawnOption "HLAR", "pepper_weaponspawns_1", "YesNo", 2
	PepperSpawnOption "Lotus Carbine", "pepper_weaponspawns_1", "YesNo", 3
	PepperSpawnOption "Box Cannon", "pepper_weaponspawns_1", "YesNo", 4
	PepperSpawnOption "Ruby Maser", "pepper_weaponspawns_1", "YesNo", 5
	PepperSpawnOption "Guillotine", "pepper_weaponspawns_1", "YesNo", 6
	PepperSpawnOption "Irons Liberator", "pepper_weaponspawns_1", "YesNo", 7
	PepperSpawnOption "Scoped Slayer", "pepper_weaponspawns_1", "YesNo", 8
	PepperSpawnOption "Helzing", "pepper_weaponspawns_1", "YesNo", 9
	PepperSpawnOption "Greely", "pepper_weaponspawns_1", "YesNo", 10
	PepperSpawnOption "Sawed-Off Slayer", "pepper_weaponspawns_1", "YesNo", 11
	PepperSpawnOption "Vera", "pepper_weaponspawns_1", "YesNo", 12
	PepperSpawnOption "Wiseau", "pepper_weaponspawns_1", "YesNo", 13
	PepperSpawnOption "Lisa", "pepper_weaponspawns_1", "YesNo", 14
	PepperSpawnOption "Oddball", "pepper_weaponspawns_1", "YesNo", 15
	PepperSpawnOption "Bastard", "pepper_weaponspawns_1", "YesNo", 16
	PepperSpawnOption "Scoped Revolver", "pepper_weaponspawns_1", "YesNo", 17
	PepperSpawnOption "BreakerTek P90", "pepper_weaponspawns_1", "YesNo", 18
	PepperSpawnOption "Mauler", "pepper_weaponspawns_1", "YesNo", 19
	PepperSpawnOption "Plasma Pistol", "pepper_weaponspawns_1", "YesNo", 20
	PepperSpawnOption "Yeyuze Grenade Launcher", "pepper_weaponspawns_1", "YesNo", 21
	PepperSpawnOption "Zorcher", "pepper_weaponspawns_1", "YesNo", 22
	PepperSpawnOption "BPX Pistol Caliber Carbine", "pepper_weaponspawns_1", "YesNo", 23
	PepperSpawnOption "ZM94 Anti-Materiel Rifle", "pepper_weaponspawns_1", "YesNo", 24
	PepperSpawnOption "Fulgur Laser Machinegun", "pepper_weaponspawns_1", "YesNo", 25
	PepperSpawnOption "Baileya Repeater 5", "pepper_weaponspawns_1", "YesNo", 26
	PepperSpawnOption "Aurochs .451 Revolver", "pepper_weaponspawns_1", "YesNo", 27
	PepperSpawnOption "PG667 Designated Marksman Rifle", "pepper_weaponspawns_1", "YesNo", 28
	PepperSpawnOption "Yurei High-Capacity SMG", "pepper_weaponspawns_1", "YesNo", 29
	Command "Enable All", "PPR_EnableAll_Weapons"
	Command "Disable All", "pepper_weaponspawns_1 0"
	StaticText ""
	StaticText "----- Ammo Spawns -----", "Teal"
	PepperSpawnOption ".500 S&W", "pepper_ammospawns_1", "YesNo", 0
	PepperSpawnOption "Trog Mags", "pepper_ammospawns_1", "YesNo", 1
	PepperSpawnOption "9mm Clips", "pepper_ammospawns_1", "YesNo", 2
	PepperSpawnOption "NDM Ammo", "pepper_ammospawns_1", "YesNo", 3
	PepperSpawnOption ".355 Mags", "pepper_ammospawns_1", "YesNo", 4
	PepperSpawnOption "12 Gauge Slugs", "pepper_ammospawns_1", "YesNo", 5
	PepperSpawnOption "ZM94 Mags", "pepper_ammospawns_1", "YesNo", 6
	PepperSpawnOption "BreakerTek P90 Mags", "pepper_ammospawns_1", "YesNo", 7
	PepperSpawnOption ".451 Frei Boxes", "pepper_ammospawns_1", "YesNo", 8
	PepperSpawnOption ".066 Bore Boxes", "pepper_ammospawns_1", "YesNo", 9
	PepperSpawnOption "Yurei Mags", "pepper_ammospawns_1", "YesNo", 10
	
	Command "Enable All", "PPR_EnableAll_Ammo"
	Command "Disable All", "pepper_ammospawns_1 0"
}
